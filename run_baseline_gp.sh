#!/bin/bash

#SBATCH --ntasks=2
#SBATCH --mem-per-cpu=32768
#SBATCH --time=4:00:00
#SBATCH --job-name=insar-superresolution-baseline_gp
#SBATCH --output=slurm/baseline_gp.out
#SBATCH --mail-type=END,FAIL
#SBATCH --account=s_stud_infk

module load gcc/8.2.0 python/3.10.4
module load eth_proxy
python baseline_gp_script.py