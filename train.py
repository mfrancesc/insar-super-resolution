import torch
import numpy as np
from tqdm import tqdm

import loss
import tgnn4i.utils

def train_epoch(model, data_loader, opt, pred_dist, config, loss_weighter):
    model.train(True)

    batch_losses = []

    for batch in tqdm(data_loader):
        batch = batch.to(config["device"]) # Move all graphs to GPU

        # some preprocessing to work with tgnn4i
        cur_batch_size = batch.batch_size
        # sort by batch
        sorted_idx = torch.sort(batch.batch, stable=True).indices
        batch.y = batch.y[sorted_idx]
        batch.mask = batch.mask[sorted_idx]
        batch.delta_t = batch.delta_t[sorted_idx]
        batch.hop_mask = batch.hop_mask[sorted_idx]
        batch.update_delta_t = batch.update_delta_t[sorted_idx]
        batch.n_id = batch.n_id[sorted_idx]
        batch.batch = batch.batch[sorted_idx]
        batch.features = batch.features[sorted_idx]
        # rename edges
        index_mapping = {old_index.item(): new_index for new_index, old_index in enumerate(sorted_idx)}
        mapped_edges = batch.edge_index.clone().cpu().apply_(lambda x: index_mapping[x])
        batch.edge_index = mapped_edges.to(batch.edge_index.device)

        batch.t = batch.t.repeat(batch.batch_size, 1) # (B, N_T)
        batch.num_graphs = batch.batch_size

        obs_mask = batch.mask.transpose(0,1) # (N_T, B*N)
        #train_ids = train_ind[batch.input_id].to(config['device'])
        #training_mask = torch.isin(batch.n_id, train_ids)
        #creat train manually
        training_mask = torch.zeros(batch.n_id.shape[0], dtype=torch.bool, device=config['device'])
        training_mask[::config['insar_k'] + config['gnss_to_insar_k'] +1] = True
        #print(training_mask.sum())
        #print(batch.batch_size)
        assert training_mask.sum() == batch.batch_size
        obs_mask[:,~training_mask] = 0 # only evaluate on the 'center' of each graph

        opt.zero_grad()

        full_pred_params, pred_delta_times = model.forward(
                batch) # (N_T, B*N, max_pred, d_y, d_param) and (N_T, B, max_pred)

        batch_loss = loss.full_future_loss(full_pred_params, batch.y, pred_delta_times,
                obs_mask, pred_dist, loss_weighter, config, metric=config['metric'])

        batch_loss.backward()
        opt.step()

        batch_losses.append(batch_loss.detach()*cur_batch_size)

    # Here mean over samples, to not weight samples in small batches higher
    epoch_loss = torch.sum(torch.stack(batch_losses))/len(data_loader.dataset)

    return epoch_loss.item()


@torch.no_grad()
def val_epoch(model, data_loader, pred_dist, loss_weighter, config):
    model.train(False)

    batch_metrics = {
        "nll": [],
        "mse": [],
    }
    for batch in tqdm(data_loader):
        batch = batch.to(config["device"]) # Move all graphs to GPU

        # some preprocessing to work with tgnn4i
        cur_batch_size = batch.batch_size
        # sort by batch
        sorted_idx = torch.sort(batch.batch, stable=True).indices
        batch.y = batch.y[sorted_idx]
        batch.mask = batch.mask[sorted_idx]
        batch.delta_t = batch.delta_t[sorted_idx]
        batch.hop_mask = batch.hop_mask[sorted_idx]
        batch.update_delta_t = batch.update_delta_t[sorted_idx]
        batch.n_id = batch.n_id[sorted_idx]
        batch.batch = batch.batch[sorted_idx]
        # rename edges
        index_mapping = {old_index.item(): new_index for new_index, old_index in enumerate(sorted_idx)}
        mapped_edges = batch.edge_index.clone().cpu().apply_(lambda x: index_mapping[x])
        batch.edge_index = mapped_edges.to(batch.edge_index.device)

        batch.t = batch.t.repeat(batch.batch_size, 1) # (B, N_T)
        batch.num_graphs = batch.batch_size

        obs_mask = batch.mask.transpose(0,1) # (N_T, B*N)
        #train_ids = val_ind[batch.input_id].to(config['device'])
        #val_mask = torch.isin(batch.n_id, train_ids)
        #creat val manually
        val_mask = torch.zeros(batch.n_id.shape[0], dtype=torch.bool, device=config['device'])
        val_mask[::config['insar_k'] + config['gnss_to_insar_k'] +1] = True
        assert val_mask.sum() == batch.batch_size
        obs_mask[:,~val_mask] = 0 # only evaluate on the 'center' of each graph

        full_pred_params, pred_delta_times = model.forward(
                batch) # (N_T, B*N, max_pred, d_y, d_param) and (N_T, B, max_pred)

        batch_nll = loss.full_future_loss(full_pred_params, batch.y, pred_delta_times,
                obs_mask, pred_dist, loss_weighter, config, metric="nll")
        batch_mse = loss.full_future_loss(full_pred_params, batch.y, pred_delta_times,
                obs_mask, pred_dist, loss_weighter, config, metric="mse")

        for val, name in zip((batch_nll, batch_mse), ("nll", "mse")):
            batch_metrics[name].append(val.detach()*cur_batch_size)

    epoch_metrics = {name:
            (torch.sum(torch.stack(val_list))/len(data_loader.dataset)).item()
            for name, val_list in batch_metrics.items()}

    return epoch_metrics