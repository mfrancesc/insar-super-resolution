import torch
import os

if __name__ == "__main__":
    n_insar = 218637

    save_path = os.path.join("data", "InSAR_data")

    # Create three sets of indices
    # 1. Training indices (70%)
    # 2. Validation indices (15%)
    # 3. Testing indices (15%)
    torch.manual_seed(0)
    perm = torch.randperm(n_insar)
    train_end = int(0.7*n_insar)
    val_end = int(0.85*n_insar)

    train_ind = perm[:train_end]
    val_ind = perm[train_end:val_end]
    test_ind = perm[val_end:]

    torch.save(train_ind, os.path.join(save_path, "train_ind.pt"))
    torch.save(val_ind, os.path.join(save_path, "val_ind.pt"))
    torch.save(test_ind, os.path.join(save_path, "test_ind.pt"))
