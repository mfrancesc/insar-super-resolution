import numpy as np
import geopandas as gpd
from readers.InSAR import InSAR
from readers.GNSS import GNSS

insar = InSAR('data/time_series_Saarland_Orbit139_update.nc')
insar_coords = insar.get_df()[['longitude', 'latitude']].values
geometry = gpd.points_from_xy(insar_coords[:,0], insar_coords[:,1])
geometry.crs = "EPSG:4326"
geometry_conv = geometry.to_crs("EPSG:32632") # https://epsg.io/32632
insar_points = np.array([geometry_conv.x, geometry_conv.y]).T

master_day = insar.get_master_day()
gnss = GNSS('data/selected_stations_dates.csv', master_day)
site_gdf = gnss.get_sites_gdf()
site_gdf = site_gdf.to_crs("EPSG:32632") # https://epsg.io/32632
gnss_points = np.array([site_gdf.geometry.x, site_gdf.geometry.y]).T
assert (np.sort(site_gdf.index.values) == site_gdf.index.values).all()
# Compute distances
from scipy.spatial.distance import cdist
distances = cdist(insar_points, gnss_points)

# save
np.save('/cluster/scratch/mfrancesc/insar_gnss_distances.npy', distances)