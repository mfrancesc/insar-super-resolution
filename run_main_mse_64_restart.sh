#!/bin/bash

#SBATCH --ntasks=2
#SBATCH --mem-per-cpu=32768
#SBATCH --time=145:00:00
#SBATCH --job-name=insar-superresolution-mse-64
#SBATCH --output=slurm/training_mse_64.out
#SBATCH --mail-type=END,FAIL
#SBATCH --gpus=rtx_4090:1
#SBATCH --account=s_stud_infk

module load gcc/8.2.0 python_gpu/3.10.4
module load eth_proxy
python main.py --batch_size 46 --graph_path /cluster/scratch/mfrancesc/DSLab/graph_10_1.pt --wandb_name mse_64 --hidden_dim 64 --load wandb/run-20231208_124224-a8iov4dx/files/epoch_20.pt --restart_step 21