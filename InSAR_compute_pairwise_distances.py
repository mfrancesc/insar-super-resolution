from tqdm import tqdm
import geopandas as gpd
import numpy as np
from scipy.spatial.distance import pdist, squareform
import warnings
from readers.InSAR import InSAR
from readers.GNSS import GNSS
from readers.util import warning_on_one_line_with_timestamp
tqdm.pandas()
warnings.formatwarning = warning_on_one_line_with_timestamp


def computed_condensed_index(i,j, m):
    assert i < j
    return m * i + j - ((i + 2) * (i + 1)) // 2

def compute_all_indices(i, m):
    l = []
    for j in range(m):
        if i == j:
            continue
        elif i < j:
            l.append(computed_condensed_index(i,j,m))
        else:
            l.append(computed_condensed_index(j,i,m))
    assert len(l) == m-1
    return l

insar = InSAR('data/time_series_Saarland_Orbit139_update.nc')
points = insar.get_df()[['longitude', 'latitude']].values

gnss_station = 'DILL'
if gnss_station is not None:
    n_points_to_keep = 5000
    master_day = insar.get_master_day()
    gnss = GNSS('data/selected_stations_dates.csv', master_day)
    site_gdf = gnss.get_sites_gdf().loc[gnss_station]
    site_lat = site_gdf["_latitude(deg)"]
    site_long = site_gdf["_longitude(deg)"]
    warnings.warn(f"Using GNSS station {gnss_station} at {site_lat}, {site_long}")
    selected_insar = insar.get_k_closest_points(site_long, site_lat, k = n_points_to_keep)
    points = selected_insar[['longitude', 'latitude']].values
    # save selected insar index numbers
    np.save(f'/cluster/scratch/mfrancesc/selected_insar_{gnss_station}.npy', selected_insar.index.values)


geometry = gpd.points_from_xy(points[:,0], points[:,1])
geometry.crs = "EPSG:4326"

geometry_conv = geometry.to_crs("EPSG:32632") # https://epsg.io/32632

gdf_conv = gpd.GeoDataFrame(geometry=geometry_conv)
warnings.warn("Starting computation of pairwise distances")
distances_condensed = pdist(np.array([gdf_conv.geometry.x, gdf_conv.geometry.y]).T)
warnings.warn("Finished computation of pairwise distances")

warnings.warn("Starting computation of nearest neighbors")
n_points = points.shape[0]
max_neighbors = 1000
results_dist = np.zeros((n_points, max_neighbors))
results_ind = np.zeros((n_points, max_neighbors))
for i in tqdm(range(n_points)):
    indices = compute_all_indices(i, points.shape[0])
    values = distances_condensed[indices]
    values = np.insert(values,i,0) # to deal with the same node
    argsort = np.argsort(values)
    results_dist[i] = values[argsort[1:max_neighbors+1]]
    results_ind[i] = argsort[1:max_neighbors+1]
warnings.warn("Finished computation of nearest neighbors")
# convert ind to int
results_ind = results_ind.astype(int)
# Save results (in scratch)
if gnss_station is not None:
    np.save(f'/cluster/scratch/mfrancesc/nearest_neighbors_dist_{gnss_station}.npy', results_dist)
    np.save(f'/cluster/scratch/mfrancesc/nearest_neighbors_ind_{gnss_station}.npy', results_ind)
else:
    np.save('/cluster/scratch/mfrancesc/nearest_neighbors_dist.npy', results_dist)
    np.save('/cluster/scratch/mfrancesc/nearest_neighbors_ind.npy', results_ind)