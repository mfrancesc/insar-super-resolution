# InSAR-super-resolution
This is the repository of Data Science Lab project "Deep-learning-based super resolution of InSAR deformation data using GNSS time series". It contains all scripts used for preprocessing and analyzing data, experimentation, training models and running baselines.

Authors:
* Jakub Lucki (jlucki@student.ethz.ch)
* Francesc Marti Escofet (mfrancesc@student.ethz.ch)

## Repository structure

* `bi_tgnn4i/` contains bidirectional variant of TGNN4I architecture
* `GPU_jupyter/` contains useful script for running jupyter notebooks on Euler
* `readers/` contains classes used to read the two types of data and perfom various preprocessing steps on them
* `scratch/` contains several notebooks used for development and exploration
* `tgnn4i/` contains the original architecture as defined in the paper "Temporal Graph Neural Networks for Irregular Data"
* `"_tests"` all files containing this string were used for development and are not crucial to end-to-end functioning of our project
* `*.sh` files were used to submit jobs to Euler cluster
* `loss.py, train.py, main.py, utils.py` are the most important files used for training our models

## Weights and Biases
The project makes use of Weights and Biases hence if you install it on your machine and log into it, you can easily monitor the training of your models.

## Necessary data
The data directory should look like this:
```
data
├── full_gnss_data.parquet #not needed if already processed
├── GNSS_data.zip #not needed if already processed
├── InSAR_data
│   ├── nearest_neighbors_dist.npy
│   ├── nearest_neighbors_ind.npy
│   ├── train_ind.pt
│   ├── test_ind.pt
│   ├── val_ind.pt
│   └── time_series_Saarland_Orbit139_update.nc
├── insar_gnss_distances.npy
├── README_GNSS_data.txt
├── selected_stations_dates.csv
└── selected_stations_dates_longer.csv
```
You can download this data from polybox using this link: [https://polybox.ethz.ch/index.php/s/lcf7EIJctz6iWDO](https://polybox.ethz.ch/index.php/s/lcf7EIJctz6iWDO)

## Euler cluster
To run the python files on Euler, we need to load python with the following commands:
```bash
module load gcc/8.2.0 python/3.10.4
```
Use python 3.10.4 to ensure compatibility with [JupyterHub](https://scicomp.ethz.ch/wiki/JupyterHub)
Maybe it would make sense to migrate to python 3.11 as it has torch 2.0 installed?
To run the visualization notebook on Euler we need to locally upgrade/install some libraries:
```bash
module load gcc/8.2.0 python/3.10.4
pip install --upgrade --user geopandas
pip install --upgrade --user rtree
pip install --user folium
pip install --upgrade --user numba
pip install --upgrade --user contextily
pip install --upgrade --user osmnx shapely
pip install --upgrade --user torch-geometric
pip install pyg-lib -f https://data.pyg.org/whl/torch-1.11.0+cu113.html
pip install --upgrade --user wandb
```

To run on Euler:
interactive:
```bash
module load gcc/8.2.0 python/3.10.4
module load eth_proxy
python main.py
```

To be able to use GPU we need to do the following:
```bash
module load gcc/8.2.0 python_gpu/3.11.2
pip install --user torch_geometric
```
To be able to run the notebooks as before we need to upgrade also the packages for python 3.11:
```bash
pip install --upgrade --user geopandas
pip install --user folium
pip install --upgrade --user osmnx shapely
pip install --upgrade --user rasterio
pip install --upgrade --user contextily
```

I set up a script to run jupyter with python_gpu. You need to do the following steps from your **local** computer, not Euler. You need to give it execution permission:
```bash
cd GPU_jupyter
chmod 755 start_jupyter_nb_gpu_3.11.sh
```
Then you need to modify the config file to use your credentials you need to modify `USERNAME`, `JNB_SSH_KEY_PATH` and `JNB_WORKING_DIR` and then use the following command to run the notebook server:
```bash
./start_jupyter_nb_gpu_3.11.sh -c jnb_config_gpu_3.11
```
All this is based on [this repo](https://gitlab.ethz.ch/sfux/Jupyter-on-Euler-or-Leonhard-Open)