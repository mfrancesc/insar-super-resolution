import netCDF4 as nc
import numpy as np
import os
import pandas as pd
from tqdm import tqdm
import zipfile
from datetime import datetime
import geopandas as gpd

def import_data(filepath = 'data/InSAR_data/time_series_Saarland_Orbit139_update.nc'):
    fn = filepath
    ds = nc.Dataset(fn)
    disp = np.array(ds['disp'][:]).T
    lat =  np.array(ds['lat'][:]).T
    lon =  np.array(ds['lon'][:]).T
    insar_days = np.array(ds['day'][:]).T
    return disp, lat, lon, insar_days

def read_insar(filepath = 'data/InSAR_data/time_series_Saarland_Orbit139_update.nc'):
    disp, lat, lon, insar_days = import_data(filepath)
    data = np.column_stack((lon, lat, disp))
    insar_days = insar_days - 730486 # convert to days since 2000-01-01
    column_names = ["longitude", "latitude"] + ["day_" + str(int(i)) for i in insar_days]
    insar_df = pd.DataFrame(data, columns=column_names)
    return insar_df

def get_insar_days(filepath = 'data/InSAR_data/time_series_Saarland_Orbit139_update.nc'):
    fn = filepath
    ds = nc.Dataset(fn)
    insar_days = np.array(ds['day'][:]).T
    insar_days = insar_days - 730486 # convert to days since 2000-01-01
    return insar_days

def get_insar_master_day(filepath = 'data/InSAR_data/time_series_Saarland_Orbit139_update.nc'):
    fn = filepath
    ds = nc.Dataset(fn)
    insar_master_day = int(ds['master_day'][:])
    insar_master_day = insar_master_day - 730486 # convert to days since 2000-01-01
    return insar_master_day

def read_gnss(path):
    # Better to use the compressed version as it is much smaller and it takes the same time to read
    files = os.listdir(path)
    files.sort()
    data = []
    for f in tqdm(files):
        if f.endswith('.tenv3'): # this won't work as there are two files which don't have this extension
            data.append(pd.read_csv(os.path.join(path, f), delim_whitespace=True))
    return data

def read_gnss_compressed(path):
    data = []
    with zipfile.ZipFile(path, 'r') as zip_ref:
        names = zip_ref.namelist()
        names = [name for name in names if not name.endswith('/')] # use only files, not directories
        for f in tqdm(names):
            with zip_ref.open(f) as myfile:
                data.append(pd.read_csv(myfile, delim_whitespace=True))
    return data

def read_gnss_parquet(path, engine='auto'):
    full_gnss_data_read = pd.read_parquet(path, engine=engine)
    # add 360 to the longitude to make it consistent with the InSAR data
    full_gnss_data_read['_longitude(deg)'] = full_gnss_data_read['_longitude(deg)'] + 360
    return full_gnss_data_read


def read_selected_gnss(path='data/selected_stations_dates.csv'):
    selected_gnss_data = pd.read_csv(path)
    return selected_gnss_data

def get_gdf(df, lon_col, lat_col, crs='EPSG:4326'):
    geometry = gpd.points_from_xy(df[lon_col], df[lat_col])
    gdf = gpd.GeoDataFrame(df, geometry=geometry, crs=crs)
    return gdf

def get_sites_gdf_gnss(gnss_data):
    gnss_sites = gnss_data.groupby('site').agg({'_longitude(deg)': 'mean', '_latitude(deg)': 'mean',})
    gnss_sites = gnss_sites.reset_index()
    gnss_sites = get_gdf(gnss_sites, '_longitude(deg)', '_latitude(deg)')
    return gnss_sites


if __name__ == '__main__':
    disp, lat, lon = import_data('data/InSAR_data/time_series_Saarland_Orbit139_update.nc')

    #GNSS_data = read_GNSS('data/GNSS_data/tenv3_IGS14_1day')

    GNSS_data = read_gnss_compressed('data/GNSS_data.zip')

