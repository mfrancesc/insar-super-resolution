clear
clc
ncfile = 'InSAR_data/time_series_Saarland_Orbit139_update.nc';
ncinfo(ncfile)
ncdisp(ncfile)
displacement = ncread(ncfile, 'disp'); % unit: mm
lataitude = ncread(ncfile, 'lat'); % unit: degrees
longitude = ncread(ncfile, 'lon'); % unit: degrees
day = ncread(ncfile, 'day'); % unit: days since 01-Jan-0000; you need to convert to year using the forlmua day/(365.25) 

