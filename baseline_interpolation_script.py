import argparse
import os
import json
import numpy as np
import torch
import torch_geometric as ptg
import wandb
import time
import pickle
import copy
import loss
from tqdm import tqdm

from utils import build_graph
from readers.InSAR import InSAR
from readers.GNSS import GNSS
from readers.util import warning_on_one_line_with_timestamp
import warnings
warnings.formatwarning = warning_on_one_line_with_timestamp

import tgnn4i.constants as constants
import tgnn4i.pred_dists as pred_dists
from tgnn4i.gru_model import GRUModel
from tgnn4i.gru_node_model import GRUNodeModel
from tgnn4i.gru_graph_model import GRUGraphModel
from tgnn4i.transformer import TransformerForecaster
from tgnn4i.transformer_joint import TransformerJointForecaster
from tgnn4i.utils import parse_loss_weight
import train


MODELS = {
    "grud_joint": GRUModel, # Ignore graph structure, evolve single joint latent state
    "grud_node": GRUNodeModel, # Treat each node independently, independent latent state
    "tgnn4i": GRUGraphModel, # Utilizes graph structure
    "transformer_node": TransformerForecaster,
    "transformer_joint": TransformerJointForecaster,
}


def get_config(args):
    parser = argparse.ArgumentParser(description='Train Models')
    # If config file should be used
    parser.add_argument("--config", type=str, help="Config file to read run config from")

    # Dataset
    parser.add_argument("--insar_dataset", type=str, default='data/InSAR_data/time_series_Saarland_Orbit139_update.nc',
            help="Path to insar dataset")
    parser.add_argument("--gnss_dataset", type=str, default='data/selected_stations_dates.csv',
            help="Path to gnss dataset")
    parser.add_argument("--dataset_type", type=str, default='derivative',
            help="Type of dataset to use (derivative/filtered/raw)")
    parser.add_argument("--filter_scale", type=float, default=5.,
            help="Filter scale for derivative/filtered data")
    parser.add_argument("--insar_k", type=int, default=10,
            help="Number of nearest neighbors to use for insar nodes")
    parser.add_argument("--gnss_to_insar_k", type=int, default=1,
            help="Number of nearest neighbors to use for gnss nodes")
    parser.add_argument("--insar_nn_ind_path", type=str, default='data/InSAR_data/nearest_neighbors_ind.npy',
            help="Path to insar nearest neighbor indices")
    parser.add_argument("--insar_nn_dist_path", type=str, default='data/InSAR_data/nearest_neighbors_dist.npy',
            help="Path to insar nearest neighbor distances")
    parser.add_argument("--gnss_insar_dist_path", type=str, default='data/insar_gnss_distances.npy',
            help="Path to gnss insar distances")
    parser.add_argument("--dtype", type=str, default="float32",
            help="Data type to use for torch tensors")
    parser.add_argument("--graph_path", type=str, default="",
                        help="Path to graph to use (overrides dataset)")
    parser.add_argument("--ind_path", type=str, default="data/InSAR_data",
                        help="Path to indices for train/val/test split")
    
    # General
    parser.add_argument("--model", type=str, default="tgnn4i",
            help="Which model to use")
    parser.add_argument("--seed", type=int, default=42,
            help="Seed for random number generator")
    parser.add_argument("--optimizer", type=str, default="adam",
            help="Optimizer to use for training")
    parser.add_argument("--init_points", type=int, default=1,
            help="Number of points to observe before prediction start")
    parser.add_argument("--test", type=int, default=0,
            help="Also evaluate on test set after training is done")
    parser.add_argument("--use_features", type=int, default=1,
            help="If additional input features should be used")
    parser.add_argument("--load", type=str,
            help="Load model parameters from path")
    parser.add_argument("--wandb_name", type=str, default=None,
            help="Name of wandb run")

    # Model Architecture
    parser.add_argument("--gru_layers", type=int, default=1,
            help="Layers of GRU units")
    parser.add_argument("--decay_type", type=str, default="dynamic",
            help="Parametrization of GRU decay to use (none/to_const/dynamic)")
    parser.add_argument("--periodic", type=int, default=0,
            help="If latent state dynamics should include periodic component")
    parser.add_argument("--time_input", type=int, default=1,
            help="Concatenate time (delta_t) to the input at each timestep")
    parser.add_argument("--mask_input", type=int, default=1,
            help="Concatenate the observation mask as input")
    parser.add_argument("--hidden_dim", type=int, default=32,
            help="Dimensionality of hidden state in GRU units (latent node state))")
    parser.add_argument("--n_fc", type=int, default=2,
            help="Number of fully connected layers after GRU units")
    parser.add_argument("--pred_gnn", type=int, default=1,
            help="Number of GNN-layers to use in predictive part of model")
    parser.add_argument("--gru_gnn", type=int, default=1,
            help="Number of GNN layers used for GRU-cells")
    parser.add_argument("--gnn_type", type=str, default="graphconv",
            help="Type of GNN-layers to use")
    parser.add_argument("--node_params", type=int, default=0, # I think it should be 0 if each graph has different nodes
            help="Use node-specific parameters for initial state and decay target")
    parser.add_argument("--learn_init_state", type=int, default=0, # I think it should be 0 if each graph has different nodes
            help="If the initial state of GRU-units should be learned (otherwise 0)")

    # Training
    parser.add_argument("--epochs", type=int,
            help="How many epochs to train for", default=50)
    parser.add_argument("--val_interval", type=int, default=1,
            help="Evaluate model every val_interval:th epoch")
    parser.add_argument("--patience", type=int, default=20,
            help="How many evaluations to wait for improvement in val loss")
    parser.add_argument("--pred_dist", type=str, default="gauss_fixed",
            help="Predictive distribution")
    parser.add_argument("--metric", type=str, default="mse",
            help="Metric to use for evaluation (mse/nll)")
    parser.add_argument("--lr", type=float,
            help="Learning rate", default=1e-3)
    parser.add_argument("--l2_reg", type=float,
            help="L2-regularization coefficient", default=0.)
    parser.add_argument("--batch_size", type=int,
            help="Batch size", default=32)
    parser.add_argument("--state_updates", type=str, default="obs",
            help="When the node state should be updated (all/obs/hop)")
    parser.add_argument("--loss_weighting", type=str, default="const",
            help="Function to weight loss with, given as: name,param1,...,paramK")
    parser.add_argument("--max_pred", type=int, default=12,
            help="Maximum number of time indices forward to predict")

    args = parser.parse_args(args)
    config = vars(args)

    # Read additional config from file
    if args.config:
        assert os.path.exists(args.config), "No config file: {}".format(args.config)
        with open(args.config) as json_file:
            config_from_file = json.load(json_file)

        # Make sure all options in config file also exist in argparse config.
        # Avoids choosing wrong parameters because of typos etc.
        unknown_options = set(config_from_file.keys()).difference(set(config.keys()))
        unknown_error = "\n".join(["Unknown option in config file: {}".format(opt)
            for opt in unknown_options])
        assert (not unknown_options), unknown_error

        config.update(config_from_file)

    # Some asserts
    # Some asserts
    assert config["model"] in MODELS, f"Unknown model: {config['model']}"
    assert config["optimizer"] in constants.OPTIMIZERS, (
            f"Unknown optimizer: {config['optimizer']}")
    assert config["pred_dist"] in pred_dists.DISTS, (
            f"Unknown predictive distribution: {config['pred_dist']}")
    assert config["gnn_type"] in constants.GNN_LAYERS, (
            f"Unknown gnn_type: {config['gnn_type']}")
    assert config["init_points"] > 0, "Need to have positive number of init points"
    assert (not bool(config["periodic"])) or (config["hidden_dim"] % 2 == 0), (
            "hidden_dim must be even when using periodic latent dynamics")
    if config["dtype"] == "float16":
        config["dtype"] = torch.float16
    elif config["dtype"] == "float32":
        config["dtype"] = torch.float32
    else:
        raise ValueError(f"Unknown dtype: {config['dtype']}")

    return config

config = get_config('')

# Set all random seeds
np.random.seed(config["seed"])
torch.manual_seed(config["seed"])

# Device setup
if torch.cuda.is_available():
    device = torch.device("cuda")

    # For reproducability on GPU
    torch.backends.cudnn.deterministic = True
    torch.backends.cudnn.benchmark = False
else:
    device = torch.device("cpu")
warnings.warn(f"Using device: {device}")

with open(r"/cluster/scratch/jlucki/insar_super_resolution/graph_10_1.pt",'rb') as f:
    graph = pickle.load(f)
    

config["num_nodes"] = 1 + config["insar_k"] + config["gnss_to_insar_k"]
config["time_steps"] = graph.t.shape[1]
config["device"] = device
config["y_dim"] = graph.y.shape[-1]

config["has_features"] = hasattr(graph, "features") and\
    bool(config["use_features"])
if config["has_features"]:
    config["feature_dim"] = graph.features.shape[-1]
else:
    config["feature_dim"] = 0

# param_dim is number of parameters in predictive distribution
pred_dist, config["param_dim"] = pred_dists.DISTS[config["pred_dist"]]

print("Config:")
for key, val in config.items():
    print(f"{key}: {val}")

# Parse loss weighting function
loss_weight_func = parse_loss_weight(config["loss_weighting"])

# Create model, optimizer
model = MODELS[config["model"]](config).to(device)
if config["load"]:
    model.load_state_dict(torch.load(config["load"], map_location=device))
    print(f"Parameters loaded from: {config['load']}")
opt = constants.OPTIMIZERS[config["optimizer"]](model.parameters(), lr=config["lr"],
        weight_decay=config["l2_reg"])

train_ind = torch.load(os.path.join(config["ind_path"], "train_ind.pt"))
val_ind = torch.load(os.path.join(config["ind_path"], "val_ind.pt"))
test_ind = torch.load(os.path.join(config["ind_path"], "test_ind.pt"))

pred_dist, config["param_dim"] = pred_dists.DISTS[config["pred_dist"]]
loss_weight_func = parse_loss_weight(config["loss_weighting"])
test_loader = ptg.loader.NeighborLoader(graph,
                                        num_neighbors=[config["insar_k"]+config["gnss_to_insar_k"]],
                                        input_nodes=test_ind,
                                        batch_size=config["batch_size"],
                                        disjoint=True,
                                        shuffle=False,
                                        pin_memory=True)
model = MODELS[config["model"]](config).to(device)
np.random.seed(42)

@torch.no_grad()
def baseline_node_inter(model, data_loader, pred_dist, loss_weighter, config):

    batch_metrics = {
        "nll": [],
        "mse": [],
    }
    for i, batch in enumerate(tqdm(data_loader)):
        batch = batch.to(config["device"]) # Move all graphs to GPU

        # some preprocessing to work with tgnn4i
        cur_batch_size = batch.batch_size
        # sort by batch
        sorted_idx = torch.sort(batch.batch, stable=True).indices
        batch.y = batch.y[sorted_idx]
        batch.mask = batch.mask[sorted_idx]
        batch.delta_t = batch.delta_t[sorted_idx]
        batch.hop_mask = batch.hop_mask[sorted_idx]
        batch.update_delta_t = batch.update_delta_t[sorted_idx]
        batch.n_id = batch.n_id[sorted_idx]
        batch.batch = batch.batch[sorted_idx]
        # rename edges
        index_mapping = {old_index.item(): new_index for new_index, old_index in enumerate(sorted_idx)}
        mapped_edges = batch.edge_index.clone().cpu().apply_(lambda x: index_mapping[x])
        batch.edge_index = mapped_edges.to(batch.edge_index.device)

        batch.t = batch.t.repeat(batch.batch_size, 1) # (B, N_T)
        batch.num_graphs = batch.batch_size

        obs_mask = batch.mask.transpose(0,1) # (N_T, B*N)
        #train_ids = val_ind[batch.input_id].to(config['device'])
        #val_mask = torch.isin(batch.n_id, train_ids)
        #creat val manually
        val_mask = torch.zeros(batch.n_id.shape[0], dtype=torch.bool, device=config['device'])
        val_mask[::config['insar_k'] + config['gnss_to_insar_k'] +1] = True
        assert val_mask.sum() == batch.batch_size
        obs_mask[:,~val_mask] = 0 # only evaluate on the 'center' of each graph

        # predict linear interpolation between two closest points
        val_target_mask = obs_mask.count_nonzero(dim=0) != 0
        target_ind = val_target_mask.nonzero().squeeze()
        predictions = torch.zeros(config["time_steps"], batch.num_nodes, config["max_pred"], 1, 1)

        for ind in target_ind:
            observation_mask = batch.y[ind] != 0
            # print(batch.y[ind].squeeze()[:50])
            for t in range(config["time_steps"]):
                # get values before and after time window
                pre = observation_mask[:t+1].squeeze(1)
                post = observation_mask[t+config["max_pred"]+1:].squeeze(1)

                pre_inds = pre.nonzero().squeeze(1)
                post_inds = post.nonzero().squeeze(1) + t+config["max_pred"]+1

                # print("Indices")
                # print(pre_inds, post_inds)

                # treat nearest value from the right as a constant if there are no more observations on the left
                if len(pre_inds) == 0:
                    pre_val = batch.y[ind, post_inds[0]]
                    pre_ind = 0
                else:
                    pre_val = batch.y[ind, pre_inds[-1]]
                    pre_ind = pre_inds[-1]

                # treat nearest value from the left as a constant if there are no more observations on the right
                if len(post_inds) == 0:
                    post_val = batch.y[ind, pre_inds[-1]]
                    post_ind = config["time_steps"]
                else:
                    post_val = batch.y[ind, post_inds[0]]
                    post_ind = post_inds[0]

                # print("pre", pre_val, pre_ind)
                # print("post", post_val, post_ind)

                dt = post_ind - pre_ind
                dy = post_val - pre_val
                grad = dy/dt

                preds = [pre_val + grad*(t - pre_ind + i) for i in range(config["max_pred"])]
                # print("Interpolation")
                preds_torch = torch.tensor(preds)[..., None, None]
                # print(preds_torch)
                predictions[t, ind] = preds_torch
        
        if i == 0 or i == len(data_loader) - 1:
            _, pred_delta_times = model.forward(batch) # this is only to get pred_delta_times

        batch_mse = loss.full_future_loss(predictions, batch.y, pred_delta_times,
                obs_mask, pred_dist, loss_weighter, config, metric="mse")
        batch_nll = loss.full_future_loss(predictions, batch.y, pred_delta_times,
                obs_mask, pred_dist, loss_weighter, config, metric="nll")

        for val, name in zip((batch_nll, batch_mse), ("nll", "mse")):
            batch_metrics[name].append(val.detach()*cur_batch_size)

    epoch_metrics = {name:
            (torch.sum(torch.stack(val_list))/len(data_loader.dataset)).item()
            for name, val_list in batch_metrics.items()}

    return epoch_metrics
print("Performance of linear interpolation", baseline_node_inter(model, test_loader, pred_dist, loss_weight_func, config))