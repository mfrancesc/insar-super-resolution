import pandas as pd
import numpy as np
import geopandas as gpd
import warnings
import matplotlib.pyplot as plt
from shapely.geometry import Point, Polygon
from geographiclib.geodesic import Geodesic
from scipy.optimize import curve_fit
import torch
from tqdm import tqdm
from .util import butter_lowpass_filter, take_n_derivative, warning_on_one_line, node_t_deltas

warnings.formatwarning = warning_on_one_line

class GNSS:
    """
    This class gets a csv file from the selected stations and dates
    """
    
    def __init__(self, path, master_day = None, outlier_threshold=3, use_sigma_periodic=False):
        self.path=path
        self.dataset = pd.read_csv(path)
        self.dataset_with_outliers = self.dataset.copy(deep=True)
        self.sampling_freq = 1/(24*60*60) # once per day on average
        
        # Passing master day as an argument is equivalent to preprocessing = True
        # Standardize the dataset w.r.t. master day
        # Remove outliers 3 s.d. outside the mean
        # add column with removed periodic function
        if master_day is not None:
            self.compute_standarized_dataset_up(master_day)
            self.compute_and_remove_outliers(outlier_threshold)
            self.compute_periodic_function(use_sigma = use_sigma_periodic)
            
    def get_df(self):
        return self.dataset
    
    def get_sites_names(self):
        return list(self.dataset['site'].unique())
    
    def get_sites_gdf(self):
        gnss_sites = self.dataset.groupby('site').agg({'_longitude(deg)': 'mean', '_latitude(deg)': 'mean'})
        geometry = gpd.points_from_xy(gnss_sites['_longitude(deg)'], gnss_sites['_latitude(deg)'])
        gdf = gpd.GeoDataFrame(gnss_sites, geometry=geometry, crs="EPSG:4326")
        return gdf

    def get_gdf(self):
        geometry = gpd.points_from_xy(
            self.dataset['_longitude(deg)'], 
            self.dataset['_latitude(deg)'])
        gdf = gpd.GeoDataFrame(self.dataset, geometry=geometry, crs="EPSG:4326")
        return gdf

    
    def get_closest_site(self, long, lat):
        """
        This function returns the closest site to the given coordinates
        """
        sites_gdf = self.get_sites_gdf()
        sites_gdf['distance(m)'] = sites_gdf.apply(lambda row: Geodesic.WGS84.Inverse(lat, long, row['_latitude(deg)'], row['_longitude(deg)'])['s12'], axis=1)
        idx = sites_gdf['distance(m)'].idxmin()
        closest_site = sites_gdf.loc[idx]
        return closest_site
    
    def _get_polygon_from_bbox(self, bbox):
        polygon = Polygon([(bbox[0], bbox[1]), (bbox[2], bbox[1]), (bbox[2], bbox[3]), (bbox[0], bbox[3])])
        return polygon
    
    def get_bbox_around_site(self, site_name, margin_in_m = 100):
        sites_gdf = self.get_sites_gdf()
        site_gdf = sites_gdf.loc[site_name]
        north = Geodesic.WGS84.Direct(site_gdf["_latitude(deg)"], site_gdf["_longitude(deg)"], 0.0, margin_in_m)["lat2"]
        east = Geodesic.WGS84.Direct(site_gdf["_latitude(deg)"], site_gdf["_longitude(deg)"], 90.0, margin_in_m)["lon2"]
        south= Geodesic.WGS84.Direct(site_gdf["_latitude(deg)"], site_gdf["_longitude(deg)"], 180.0, margin_in_m)["lat2"]
        west = Geodesic.WGS84.Direct(site_gdf["_latitude(deg)"], site_gdf["_longitude(deg)"], 270.0, margin_in_m)["lon2"]
        bbox = np.array([west, south, east, north])
        polygon = self._get_polygon_from_bbox(bbox)
        return polygon
    
    def remove_outliers(self):
        self.dataset = self.dataset[~self.dataset['outlier']].copy(deep=True)
        self.dataset.drop(columns=['outlier'], inplace=True)

    def get_df_with_outliers(self):
        return self.dataset_with_outliers

    def compute_standarized_dataset_up(self, master_day, unit='mm'):
        ref_values = {}
        dataset = self.get_df()
        for site in self.get_sites_names():
            closest_day_idx = dataset[(dataset['site'] == site)]['day'].apply(lambda x: abs(x - master_day)).idxmin()
            ref_row = dataset.loc[closest_day_idx]            
            if ref_row['day'] != master_day:
                warnings.warn(f"{site} does not have data for {master_day}. Using closest day {ref_row['day']} instead.")
            ref_values[site] = ref_row

        for site, ref_row in ref_values.items():
            # subtract reference value of up component
            if unit == 'mm':
                dataset.loc[dataset['site'] == site, f'st_up(mm)'] = (dataset.loc[dataset['site'] == site, '____up(m)'] - ref_row['____up(m)']) * 1000
            elif unit == 'm':
                dataset.loc[dataset['site'] == site, f'st_up(m)'] = dataset.loc[dataset['site'] == site, '____up(m)'] - ref_row['____up(m)']
            else:
                raise ValueError('unit must be mm or m')
            
    def compute_periodic_function(self, use_sigma=False):
        #
        # I think we need to do the same for the insar data
        # Not as insar data is already corrected for the yearly effect
        #
        """
        This function computes the periodic function for each site
        and computes the values of this function for each day
        """
        def f(x,a,b,c):
            # right now this is a linear function, but we can try different functions
            return a*np.sin(x*2*np.pi / 365.25) + b*np.cos(x*2*np.pi / 365.25) + c
        sites = self.get_sites_names()
        for site in sites:
            popt, pcov = GNSS.fit_periodic_function(self.dataset, site, f, use_sigma=use_sigma)
            # apply the function to the site
            self.dataset.loc[self.dataset['site'] == site, 'periodic_func(mm)'] = f(self.dataset.loc[self.dataset['site'] == site, 'day'], *popt)

        # remove the yearly effect from the dataset
        self.dataset['st_up_no_year(mm)'] = self.dataset['st_up(mm)'] - self.dataset['periodic_func(mm)']

    def compute_and_remove_outliers(self, thresh=3):
        #
        # Do you think we should also do the same for insar? or do we trust the data is perfect?
        #
        def zscore_(x):
            return (x - x.mean()) / x.std(ddof=1)
        
        sites = self.get_sites_names()
        for site in sites:
            self.dataset.loc[self.dataset['site'] == site, 'zscore'] = zscore_(self.dataset.loc[self.dataset['site'] == site, 'st_up(mm)'])
            self.dataset.loc[self.dataset['site'] == site, 'outlier'] = self.dataset.loc[self.dataset['site'] == site, 'zscore'].abs() > thresh
        # convert outlier column to boolean
        self.dataset['outlier'] = self.dataset['outlier'].astype(bool)
        # remove row outliers
        self.remove_outliers()
    
    def filter_signal(self, freq_threshold, site_name = None, order = 5, cache = True):
        """
        Filters the displacement signal using butterpass filter, 
        using periodically corrected signal
        """
        nyq_gnss = 0.5 * self.sampling_freq
        
        # if site_name is None apply for all sites
        if site_name is None:
            sites = self.get_sites_names()
            for site in sites:
                # appends new column 'st_up_filtered(mm)', returns df with site day and filtered values
                y = self.dataset.loc[self.dataset['site'] == site, 'st_up_no_year(mm)'].values
                try:
                    filtered = butter_lowpass_filter(y, freq_threshold, nyq_gnss, order=order)
                except:
                    filtered = None
                    print(f"Filtering failed for {site}:",len(y), y)
                    print(f"Removing {site} from the dataset")
                    self.dataset.drop(self.dataset[self.dataset['site'] == site].index, inplace=True)
                self.dataset.loc[self.dataset['site'] == site, 'st_up_filtered(mm)'] = filtered
            return self.dataset[["site", "day", "st_up_filtered(mm)"]]
        else:
            if cache and 'st_up_filtered(mm)' in self.dataset.columns: # added cache option for plotting reasons, when I compute several different filteres
                return self.dataset.loc[self.dataset['site'] == site_name]
            # returns a numpy array of filtered signal
            y = self.dataset.loc[self.dataset['site'] == site_name, 'st_up_no_year(mm)'].values
            self.dataset.loc[self.dataset['site'] == site_name, 'st_up_filtered(mm)'] = butter_lowpass_filter(y, freq_threshold, nyq_gnss, order)
            return self.dataset.loc[self.dataset['site'] == site_name]
    
    def take_derivative(self, site_name = None, cache = True):
        # if site_name is None apply for all sites
        days = self.dataset["day"].values
        if site_name is None:
            sites = self.get_sites_names()
            for site in sites:
                # appends new column 'st_up_derivative(mm)', returns df with site day and filtered values
                y = self.dataset.loc[self.dataset['site'] == site, 'st_up_filtered(mm)'].values
                days = self.dataset.loc[self.dataset['site'] == site,"day"].values
                # if column is missing you need to call filter_signal first
                derivative = take_n_derivative(y=y, x = days)
                self.dataset.loc[self.dataset['site'] == site, 'st_up_derivative(mm)'] = derivative
            return self.dataset[["site", "day", "st_up_derivative(mm)"]]
        else:
            if cache and 'st_up_derivative(mm)' in self.dataset.columns:
                return self.dataset.loc[self.dataset['site'] == site_name]
            # returns a pandas df of signal after derivation
            y = self.dataset.loc[self.dataset['site'] == site_name, 'st_up_filtered(mm)'].values
            days = self.dataset.loc[self.dataset['site'] == site_name,"day"].values
            self.dataset.loc[self.dataset['site'] == site_name, 'st_up_derivative(mm)'] = take_n_derivative(y=y, x = days)
            return self.dataset.loc[self.dataset['site'] == site_name]
        
    @staticmethod
    def get_gdf(df, lon_col='_longitude(deg)', lat_col='_latitude(deg)', crs='EPSG:4326'):
        geometry = gpd.points_from_xy(df[lon_col], df[lat_col])
        gdf = gpd.GeoDataFrame(df, geometry=geometry, crs=crs)
        return gdf
    
    @staticmethod
    def plot_gnss_series(gnss_df, site, column, print_missing_dates=False):
        selected_site_df = gnss_df[gnss_df['site'] == site]
        
        min_day = selected_site_df['day'].min()
        max_day = selected_site_df['day'].max()
        complete_days_df = pd.DataFrame({'day': range(min_day, max_day + 1)})

        # Merge the two DataFrames using a left join
        merged_df = complete_days_df.merge(selected_site_df, on='day', how='left')
        
        days = merged_df['day']
        x = pd.to_datetime(days, origin='2000-01-01', unit='D')
        y = merged_df[column]
        plt.plot(x,y)
        # Find the gaps (NaN values) in the '____up(m)' column
        gaps = merged_df[column].isna()
        # Create a rectangle covering the entire height of the plot and the width of the missing values
        i = 0
        if print_missing_dates:
            print('Missing dates:')
        while i < len(gaps):
            if gaps[i]:
                start_date = x[i]
                end_date = x[i]
                while i < len(gaps) and gaps[i]:
                    end_date = x[i]
                    i += 1
                if print_missing_dates:
                    print(start_date.strftime('%Y-%m-%d'), ' - ',  end_date.strftime('%Y-%m-%d'))
                plt.axvspan(start_date, end_date, facecolor='red', alpha=0.4)
            i += 1
        plt.xticks(rotation=45)
        plt.title('GNSS data for station ' + site)
        plt.ylabel(column)

    @staticmethod
    def fit_periodic_function(gnss_df, site, f, use_sigma=False):
        selected_site_df = gnss_df[gnss_df['site'] == site]
        x = selected_site_df['day']
        y = selected_site_df['st_up(mm)']
        if use_sigma:
            sigma = selected_site_df['sig_u(m)'] * 1000
            popt, pcov = curve_fit(f, x, y, sigma=sigma)
        else:
            popt, pcov = curve_fit(f, x, y)
        return popt, pcov
    
    @staticmethod
    def get_data_for_graph(gnss_df, insar_min_day, insar_max_day, dtype=torch.float32, device='cpu'):
        """
        This function returns the data for the graph
        gnss_df: pandas dataframe with the following columns:
            site, day, value
            Where the value column is the value of the time series (can be derivative, filtered...)
        dtype: torch datatype
        device: torch device
        """
        assert 'site' in gnss_df.columns
        assert 'day' in gnss_df.columns
        assert 'value' in gnss_df.columns
        assert len(gnss_df.columns) == 3
        sites = gnss_df['site'].unique()
        assert (np.sort(sites) == sites).all()

        num_sites = len(sites)
        days = torch.arange(insar_min_day, insar_max_day+1, dtype=dtype, device=device)
        y = torch.zeros((num_sites, len(days)), dtype=dtype, device=device)
        mask = torch.zeros((num_sites, len(days)), dtype=dtype, device=device)

        for idx, site in tqdm(enumerate(sites), total=len(sites)):
            site_df = gnss_df[gnss_df['site'] == site]
            site_days = site_df['day'].values
            site_values = site_df['value'].values
            y[idx, site_days - insar_min_day] = torch.tensor(site_values, dtype=dtype, device=device)
            mask[idx, site_days - insar_min_day] = 1
        y = y.unsqueeze(2)
        ts = (days - insar_min_day) / (insar_max_day - insar_min_day)
        delta_t = node_t_deltas(ts.unsqueeze(0), mask.unsqueeze(0).transpose(1,2), dtype=dtype, device=device).squeeze(0).transpose(0,1)
        return y, mask, ts, delta_t
