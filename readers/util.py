import numpy as np
from scipy.signal import butter,filtfilt
import torch
from tqdm import tqdm

def warning_on_one_line(message, category, filename, lineno, file=None):
    return f"{filename}:{lineno}: {category.__name__}: {message}\n"
def warning_on_one_line_with_timestamp(message, category, filename, lineno, file=None):
    import datetime
    return f"{datetime.datetime.now()}: {filename}:{lineno}: {category.__name__}: {message}\n"

def butter_lowpass_filter(data, cutoff, nyq, order = 5):
    # Normalize the cutoff frequency
    normal_cutoff = cutoff / nyq
    # Get the filter coefficients 
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    y = filtfilt(b, a, data, method="gust") #method="gust"
    return y

def take_n_derivative(x:float, y:float, n:int = 1):
    """
    This is actually derivative approximation through finite difference.
    
    Returns array of length n where the first value is 0
    """
    dy = np.diff(y, n=n)
    dx = np.diff(x, n=n)
    return np.pad(dy/dx, (n,0))

# Create time deltas for individual nodes (taken from tgnn4i/utils.py)
def node_t_deltas_old(ts, obs_mask, dtype=torch.float32, device='cpu'):
    # Create time deltas
    # ts: (N_data, N_T)
    # obs_mask: (N_data, N_T, N)
    n_nodes = obs_mask.shape[2]
    repeated_t = ts.unsqueeze(2).repeat(1,1,n_nodes) # (N_samples, N_T, N)
    observed_t = repeated_t * obs_mask # (N_samples, N_T, N)
    prev_t_list = [torch.zeros(ts.shape[0], n_nodes, dtype=dtype, device=device)] # Before t_1 is just 0
    for i_t in tqdm(range(1, ts.shape[1])):
        # t_slice has shape (N_samples, N)
        # In observed_t the unobserved timesteps are just 0, while observed ones are  > 0
        prev_t_list.append(torch.max(observed_t[:,:i_t,:], dim=1)[0])

    prev_t = torch.stack(prev_t_list, dim=1)
    delta_t = repeated_t - prev_t # (N_samples, N_T, N)
    return delta_t # (N_samples, N_T, N)

# faster version of node_t_deltas
def node_t_deltas(ts, obs_mask, dtype=torch.float32, device='cpu'):
    # Create time deltas
    # ts: (N_data, N_T)
    # obs_mask: (N_data, N_T, N)
    n_nodes = obs_mask.shape[2]
    
    # Broadcasting instead of unsqueeze and repeat
    repeated_t = ts.unsqueeze(2)  # (N_data, N_T, 1)
    observed_t = repeated_t * obs_mask  # (N_data, N_T, N)
    observed_t = torch.cat([torch.zeros(ts.shape[0], 1, n_nodes, dtype=torch.float32, device=device), observed_t], axis=1) # (N_data, N_T + 1, N)

    # Calculate cumulative maximum along the time dimension
    prev_t = torch.cummax(observed_t, dim=1)[0]  # (N_data, N_T + 1, N)
    prev_t = prev_t.to(dtype)
    # Create delta_t
    delta_t = repeated_t - prev_t[:,:-1,:]  # (N_data, N_T, N)
    
    return delta_t

# taken from tgnn4i/utils.py
def compute_edge_weights(edge_dist, weight_scaling):
    # edge_dist: (N_edges,)

    # compute edge weight from distance (Following Li et al.)
    # Robust std.-estimation
    dist_mad = torch.median(torch.abs(edge_dist - torch.median(edge_dist)))
    dist_std = 1.4826*dist_mad
    edge_weight = torch.exp(-((edge_dist / (weight_scaling*dist_std))**2))

    return edge_weight