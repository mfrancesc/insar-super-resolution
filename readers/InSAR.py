import netCDF4 as nc
import numpy as np
import pandas as pd
import geopandas as gpd
from shapely.geometry import Polygon
from geographiclib.geodesic import Geodesic
import matplotlib.pyplot as plt
from .util import butter_lowpass_filter, take_n_derivative, node_t_deltas, compute_edge_weights, warning_on_one_line
from tqdm import tqdm
import torch
import torch_geometric as ptg
import warnings
from deprecated import deprecated
warnings.formatwarning = warning_on_one_line


class InSAR:
    def __init__(self, file_path, threshold=3):
        tqdm.pandas()
        self.file_path = file_path
        self.dataset = nc.Dataset(self.file_path)

        disp = np.array(self.dataset['disp'][:]).T
        lat =  np.array(self.dataset['lat'][:]).T
        lon =  np.array(self.dataset['lon'][:]).T
        insar_days = np.array(self.dataset['day'][:]).T
        data = np.column_stack((lon, lat, disp))
        insar_days = insar_days - 730486
        column_names = ["longitude", "latitude"] + ["day_" + str(int(i)) for i in insar_days]
        self.df = pd.DataFrame(data, columns=column_names)
        self.df_with_outliers = self.df.copy()
        self.compute_and_remove_outliers(thresh=threshold)
        
        self.sampling_freq = 1/(2*7*24*60*60) # 2 weeks on average
        self.filtered_df = None
        self.derivative_df = None
    
    def get_df(self):
        return self.df

    def get_master_day(self):
        insar_master_day = int(self.dataset['master_day'][:])
        insar_master_day = insar_master_day - 730486
        return insar_master_day
    
    def get_gdf(self, dataset='raw'):
        geometry = gpd.points_from_xy(self.df.longitude, self.df.latitude)
        if dataset == 'filtered':
            gdf = gpd.GeoDataFrame(self.filtered_df, geometry=geometry, crs="EPSG:4326")
        elif dataset == 'derivative':
            gdf = gpd.GeoDataFrame(self.derivative_df, geometry=geometry, crs="EPSG:4326")
        elif dataset == 'raw':
            gdf = gpd.GeoDataFrame(self.df, geometry=geometry, crs="EPSG:4326")
        else:
            raise Exception("dataset has to be 'raw', 'filtered' or 'derivative'")
        return gdf
    
    def get_days(self):
        insar_days = np.array(self.dataset['day'][:]).T
        insar_days = insar_days - 730486
        return insar_days
    
    def get_first_and_last_day(self):
        insar_days = self.get_days()
        return int(insar_days[0]), int(insar_days[-1])
    
    def _get_bbox(self):
        bbox = np.array([self.df['longitude'].min(), 
                         self.df['latitude'].min(), 
                         self.df['longitude'].max(),
                         self.df['latitude'].max()])
        return bbox

    def _get_polygon_from_bbox(self, bbox):
        polygon = Polygon([(bbox[0], bbox[1]), (bbox[2], bbox[1]), (bbox[2], bbox[3]), (bbox[0], bbox[3])])
        return polygon
    
    def get_bbox(self):
        bbox = self._get_bbox()
        polygon = self._get_polygon_from_bbox(bbox)
        return polygon
    
    def get_bbox_plus_margin(self):
        bbox = self._get_bbox()
        long_size = bbox[2] - bbox[0]
        lat_size = bbox[3] - bbox[1]
        bbox_plus_margins = bbox + np.array([-long_size, -lat_size, long_size, lat_size])
        polygon = self._get_polygon_from_bbox(bbox_plus_margins)
        return polygon
    
    def get_k_closest_points(self, long, lat, k, dataset='raw'):
        """
        This function returns k closest insar datapoints to a particular position
        """
        print(f"Searching for {k} nearest InSAR datapoints")
        points_gdf = self.get_gdf(dataset=dataset)
        points_gdf['distance(m)'] = points_gdf.progress_apply(lambda row: Geodesic.WGS84.Inverse(lat, long, row['latitude'], row['longitude'])['s12'], axis=1)
        points_gdf.sort_values(by = ['distance(m)'], inplace=True)
        return points_gdf.iloc[:k]
    
    def filter_signal(self, freq_threshold, dataset = None, lat = None, long = None, order = 5):
        """
        Filters the displacement signal using butterpass filter, 
        using periodically corrected signal
        Maybe instaed of using lat and long to identify the site, we can use index, bc it does not have decimals
        """
        print("Filtering InSAR signal...")
        # if we provide dataset we will filter it, but has to have the same structure as insar
        if dataset is not None:
            assert set(self.df.columns).issubset(set(dataset.columns)), "Dataset has to have the same structure as insar"
            print("Using provided dataset")
        else:
            print("Using raw dataset")
        df = dataset if dataset is not None else self.df
        
        nyq_insar = 0.5 * self.sampling_freq        
        days_col = [col for col in df.columns if col.startswith("day")]
        def butter_lowpass_filter_insar_nan(x, freq_threshold, nyq_insar, order):
            not_nan = ~np.isnan(x)
            x = x[not_nan]
            filtered = butter_lowpass_filter(x, freq_threshold, nyq_insar, order=order)
            # add nans back in the same place
            y = np.full(len(not_nan), np.nan)
            y[not_nan] = filtered
            return y
        # if lat or long is None apply to all
        if (lat is None or long is None) or dataset is not None:
            # returns df with filtered data
            new_df = df[days_col].progress_apply(lambda x: butter_lowpass_filter_insar_nan(x, freq_threshold, nyq_insar, order), axis = 1, raw=True)
            self.filtered_df = df[["longitude","latitude"]].join(new_df)
            return self.filtered_df
        else:
            if self.filtered_df is not None:
                # if we have already filtered the signal, we can just return the value
                y = self.filtered_df[(self.filtered_df['latitude'] == lat) & (self.filtered_df['longitude'] == long)].values
                return y
            # returns a numpy array of filtered signal
            y = df[(df['latitude'] == lat) & (df['longitude'] == long)].values
            return butter_lowpass_filter_insar_nan(y, freq_threshold, nyq_insar, order)
    
    def take_derivative(self, use_filtered=True, dataset = None, lat = None, long = None):
        """
        Same about lat and long as in filter_signal
        """
        print("Taking derivative of InSAR signal...")
        # if we provide dataset we will take derivative of it, but has to have the same structure as insar
        if dataset is not None:
            df = dataset
            assert set(self.df.columns).issubset(set(df.columns)), "Dataset has to have the same structure as insar"
            print("Using provided dataset")
        else:
            df = self.filtered_df if use_filtered else self.df
            txt = "filtered" if use_filtered else "raw"
            print(f"Using {txt} dataset")
        
        # if site_name is None apply for all sites
        days = np.array([int(col[-4:]) for col in self.df.columns if col.startswith("day")])
        days_col = [col for col in self.df.columns if col.startswith("day")]
        def take_n_derivative_insar_nan(x, y, n=1):
            not_nan = ~np.isnan(y)
            y = y[not_nan]
            x = x[not_nan]
            derivative = take_n_derivative(x, y, n)
            # add nans back in the same place
            z = np.full(len(not_nan), np.nan)
            z[not_nan] = derivative
            return z
        if (lat is None or long is None) or dataset is not None:
            # returns df with 
            new_df = df[days_col].progress_apply(lambda x: take_n_derivative_insar_nan(y = x, x = days), axis = 1, raw=True)
            self.derivative_df = df[["longitude","latitude"]].join(new_df)
            return self.derivative_df
        else:
            # not tested xd
            # returns a numpy array of filtered signal
            if self.derivative_df is not None:
                y = self.derivative_df[(self.derivative_df['latitude'] == lat) & (self.derivative_df['longitude'] == long)].values
                return y
            y = df[(df['latitude'] == lat) & (df['longitude'] == long)].values
            return take_n_derivative_insar_nan(y=y, x = days)
        
    def compute_and_remove_outliers(self, thresh=3):
        print("Computing zscores and removing outliers for InSAR...")
        def zscore_(x):
            return (x - x.mean()) / x.std(ddof=1)
        
        days_col = [col for col in self.df.columns if col.startswith("day")]

        # compute zscore for each site
        self.zscores = self.df.copy()
        self.zscores[days_col] = self.zscores[days_col].progress_apply(zscore_, axis=1)
        # remove outliers (assing NaN to outliers) in self.df
        self.df[days_col] = self.df[days_col].where(np.abs(self.zscores[days_col]) < thresh)


    @staticmethod
    def plot_insar_series(insar_df, index, ax=None):
        if ax is None:
            fig, ax = plt.subplots(figsize=(7,5))
        row = insar_df.iloc[index]
        cols = [c for c in insar_df.columns if c.startswith('day_')]
        days = [int(c[4:]) for c in cols]
        assert len(cols) == 112
        x = pd.to_datetime(days, origin='2000-01-01', unit='D')
        y = row[cols].values
        # remove nans (in case we have removed outliers)
        x = x[~np.isnan(y)]
        y = y[~np.isnan(y)]
        ax.plot(x,y)
        # change xticks rotation
        for tick in ax.get_xticklabels():
            tick.set_rotation(45)
        ax.set_title(f"InSAR data for location ({row['longitude']}, {row['latitude']})")
        ax.set_ylabel("Vertical displacement (mm)")

    @deprecated("Use utils.build_graph instead")
    @staticmethod
    def get_edges_and_distances_all2all(insar_df, threshold_distance=None, dtype=torch.float32, device='cpu'):
        '''
        Returns edges with distances for a given insar_df, edges need to be repeated in both directions
        edge_index: shape (2, N_edges)
        edge_weight: shape (N_edges,1) which is the distance between two points
        '''
        # get all points
        points = insar_df[['longitude', 'latitude']].values
        # get all edges
        edges = []
        edge_distances = []
        for i in tqdm(range(len(points))):
            for j in range(i+1, len(points)):
                # get distance between two points
                distance = Geodesic.WGS84.Inverse(points[i][1], points[i][0], points[j][1], points[j][0])['s12']
                if threshold_distance is None or distance < threshold_distance:
                    edges.append([i,j])
                    edge_distances.append(distance)
        # repeat edges in both directions
        edges = np.array(edges)
        edges = np.concatenate((edges, edges[:,::-1]), axis=0)
        edge_distances = np.array(edge_distances)
        edge_distances = np.concatenate((edge_distances, edge_distances))
        # to torch
        edges = torch.tensor(edges, dtype=torch.long, device=device).t().contiguous()
        edge_distances = torch.tensor(edge_distances, dtype=dtype, device=device).unsqueeze(1)
        return edges, edge_distances

    @deprecated("Use utils.build_graph instead")
    @staticmethod
    def get_edges_and_distances_knn(insar_df, k, dtype=torch.float32, device='cpu'):
        '''
        Returns edges with distances for a given insar_df
        edge_index: shape (2, N_edges)
        edge_weight: shape (N_edges,1) which is the distance between two points
        '''
        warnings.warn("This is not tested as there was an error and did not test it afterwards")
        # get all points
        points = insar_df[['longitude', 'latitude']].values
        # get all edges
        edges = []
        edge_distances = []
        for i in tqdm(range(len(points))):
            # get distance between two points
            distances = []
            for j in range(len(points)):
                if i != j:
                    distance = Geodesic.WGS84.Inverse(points[i][1], points[i][0], points[j][1], points[j][0])['s12']
                    distances.append(distance)
                else:
                    distances.append(0)
            # get k closest points
            closest_points = np.argsort(distances)[1:k+1] # first one is itself
            for j in closest_points:
                # add edge in direction j->i
                edges.append([j,i])
                edge_distances.append(distances[j]) 
        # to torch
        edges = torch.tensor(edges, dtype=torch.long, device=device).t().contiguous()
        edge_distances = torch.tensor(edge_distances, dtype=dtype, device=device).unsqueeze(1)
        return edges, edge_distances

    @deprecated("Use get_data_for_graph instead")
    @staticmethod
    def get_data_for_graph_non_vectorized(insar_df, dtype=torch.float32, device='cpu'):
        '''
        Returns the needed data to build the graph
        Each day is a timestep, as it's irregular and also preparing for GNSS data
        y: shape (N_nodes, N_timesteps, 1)
        mask: shape (N_nodes, N_timesteps)
        ts: shape (N_timesteps)
        delta_ts: shape (N_nodes, N_timesteps)
        '''
        cols = [c for c in insar_df.columns if c.startswith('day_')]
        insar_days = [int(c[4:]) for c in cols]
        assert len(cols) == 112
        first_day = min(insar_days)
        last_day = max(insar_days)
        points = insar_df[cols].values

        days = torch.arange(first_day, last_day+1, dtype=dtype, device=device)
        y = torch.full((len(points), len(days), 1), 0, dtype=dtype, device=device)
        mask = torch.full((len(points), len(days)), 0, dtype=dtype, device=device)
        for i in tqdm(range(len(points))):
            for j in range(first_day, last_day+1):
                if j in insar_days and not np.isnan(points[i][insar_days.index(j)]):
                    y[i][j-first_day][0] = points[i][insar_days.index(j)]
                    mask[i][j-first_day] = 1
        # compute ts between 0 and 1
        ts = (days - first_day) / (last_day - first_day)
        # compute delta_ts
        delta_ts = node_t_deltas(ts.unsqueeze(0), mask.unsqueeze(0).transpose(1,2)).squeeze(0).transpose(0,1)
        return y, mask, ts, delta_ts
    
    @staticmethod
    def get_data_for_graph(insar_df, dtype=torch.float32, device='cpu'):
        '''
        Returns the needed data to build the graph
        Each day is a timestep, as it's irregular and also preparing for GNSS data
        y: shape (N_nodes, N_timesteps, 1)
        mask: shape (N_nodes, N_timesteps)
        ts: shape (N_timesteps)
        delta_ts: shape (N_nodes, N_timesteps)
        first_day: int -> This is for GNSS data
        last_day: int
        '''
        cols = [c for c in insar_df.columns if c.startswith('day_')]
        insar_days = [int(c[4:]) for c in cols]
        insar_days = torch.tensor(insar_days)
        assert len(cols) == 112
        first_day = min(insar_days).item()
        last_day = max(insar_days).item()
        # get all points
        points = insar_df[cols].values
        points = torch.tensor(points, dtype=dtype, device=device)
        days = torch.arange(first_day, last_day+1, dtype=dtype, device=device)
        y = torch.zeros((len(points), len(days), 1), dtype=dtype, device=device)
        mask = torch.zeros((len(points), len(days)), dtype=dtype, device=device)
        y[:,insar_days - first_day] = points.unsqueeze(2)
        # put 0 in nan values
        y[torch.isnan(y)] = 0
        mask[:,insar_days - first_day] = points.isnan().logical_not().float().type(dtype)
        # compute ts between 0 and 1
        ts = (days - first_day) / (last_day - first_day)
        # compute delta_ts
        delta_ts = node_t_deltas(ts.unsqueeze(0), mask.unsqueeze(0).transpose(1,2), dtype=dtype, device=device).squeeze(0).transpose(0,1)
        return y, mask, ts, delta_ts, first_day, last_day
    
    @deprecated("Use utils.build_graph instead")
    @staticmethod
    def build_graph(insar_df, type, weight_scaling, k=None, dist_threshold=None, add_features=False, dtype=torch.float32, device='cpu'):
        if type == 'all2all':
            edges, edge_distances = InSAR.get_edges_and_distances_all2all(insar_df, threshold_distance=dist_threshold, dtype=dtype, device=device)
        elif type == 'knn':
            if k is None:
                raise Exception("k has to be specified")
            edges, edge_distances = InSAR.get_edges_and_distances_knn(insar_df, k=k, dtype=dtype, device=device)
        else:
            raise Exception("type has to be 'all2all' or 'knn'")
        y, mask, ts, delta_ts = InSAR.get_data_for_graph(insar_df, dtype=dtype, device=device)
        edge_weights = compute_edge_weights(edge_distances, weight_scaling=weight_scaling)
        
        # create graph
        num_nodes = len(insar_df)
        hop_mask = mask
        update_delta_t = delta_ts
        if add_features:
            raise Exception("Not implemented yet")
        else:
            graph = ptg.data.Data(edge_index=edges, 
                                  edge_attr=edge_weights,
                                  y=y,
                                  t = ts.unsqueeze(0),
                                  delta_t = delta_ts,
                                  mask=mask,
                                  hop_mask=hop_mask,
                                  update_delta_t=update_delta_t,
                                  num_nodes=num_nodes)
        return graph


