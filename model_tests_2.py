import numpy as np
from tqdm import tqdm
from geographiclib.geodesic import Geodesic
import torch_geometric as ptg
import torch
import warnings
import matplotlib.pyplot as plt
import argparse
import os
import json

import tgnn4i.constants as constants
import tgnn4i.pred_dists as pred_dists
from tgnn4i.gru_model import GRUModel
from tgnn4i.gru_node_model import GRUNodeModel
from tgnn4i.gru_graph_model import GRUGraphModel
from tgnn4i.transformer import TransformerForecaster
from tgnn4i.transformer_joint import TransformerJointForecaster
from tgnn4i.utils import parse_loss_weight
import utils
import pickle

from readers.InSAR import InSAR
from readers.GNSS import GNSS

USE_CACHED_GRAPH = True

MODELS = {
    "grud_joint": GRUModel, # Ignore graph structure, evolve single joint latent state
    "grud_node": GRUNodeModel, # Treat each node independently, independent latent state
    "tgnn4i": GRUGraphModel, # Utilizes graph structure
    "transformer_node": TransformerForecaster,
    "transformer_joint": TransformerJointForecaster,
}

def get_config(args=None):
    parser = argparse.ArgumentParser(description='Train Models')
    # If config file should be used
    parser.add_argument("--config", type=str, help="Config file to read run config from")

    # General
    parser.add_argument("--model", type=str, default="tgnn4i",
            help="Which model to use")
    
    parser.add_argument("--insar-dataset", type=str, default='data/InSAR_data/time_series_Saarland_Orbit139_update.nc',
            help="Path to insar dataset")
    parser.add_argument("--gnss-dataset", type=str, default='data/selected_stations_dates.csv',
            help="Path to gnss dataset")
    parser.add_argument("--dataset-type", type=str, default='derivative',
            help="Type of dataset to use (derivative/filtered/raw)")
    parser.add_argument("--filter_scale", type=float, default=5.,
            help="Filter scale for derivative/filtered data")
    parser.add_argument("--insar_k", type=int, default=10,
            help="Number of nearest neighbors to use for insar nodes")
    parser.add_argument("--gnss_to_insar_k", type=int, default=1,
            help="Number of nearest neighbors to use for gnss nodes")
    parser.add_argument("--insar_nn_ind_path", type=str, default='data/InSAR_data/nearest_neighbors_ind.npy',
            help="Path to insar nearest neighbor indices")
    parser.add_argument("--insar_nn_dist_path", type=str, default='data/InSAR_data/nearest_neighbors_dist.npy',
            help="Path to insar nearest neighbor distances")
    parser.add_argument("--gnss_insar_dist_path", type=str, default='data/insar_gnss_distances.npy',
            help="Path to gnss insar distances")
    parser.add_argument("--edge_weight_scaling", type=float, default=6.,
            help="Scaling of edge weights")
    parser.add_argument("--dtype", type=str, default="float32",
            help="Data type to use for torch tensors")
    
    
    
    
    parser.add_argument("--seed", type=int, default=42,
            help="Seed for random number generator")
    parser.add_argument("--optimizer", type=str, default="adam",
            help="Optimizer to use for training")
    parser.add_argument("--test", type=int, default=0,
            help="Also evaluate on test set after training is done")
    parser.add_argument("--use_features", type=int, default=1,
            help="If additional input features should be used")
    parser.add_argument("--load", type=str,
            help="Load model parameters from path")

    # Model Architecture
    parser.add_argument("--gru_layers", type=int, default=1,
            help="Layers of GRU units")
    parser.add_argument("--decay_type", type=str, default="dynamic",
            help="Parametrization of GRU decay to use (none/to_const/dynamic)")
    parser.add_argument("--periodic", type=int, default=0,
            help="If latent state dynamics should include periodic component")
    parser.add_argument("--time_input", type=int, default=1,
            help="Concatenate time (delta_t) to the input at each timestep")
    parser.add_argument("--mask_input", type=int, default=1,
            help="Concatenate the observation mask as input")
    parser.add_argument("--hidden_dim", type=int, default=32,
            help="Dimensionality of hidden state in GRU units (latent node state))")
    parser.add_argument("--n_fc", type=int, default=2,
            help="Number of fully connected layers after GRU units")
    parser.add_argument("--pred_gnn", type=int, default=1,
            help="Number of GNN-layers to use in predictive part of model")
    parser.add_argument("--gru_gnn", type=int, default=1,
            help="Number of GNN layers used for GRU-cells")
    parser.add_argument("--gnn_type", type=str, default="graphconv",
            help="Type of GNN-layers to use")
    parser.add_argument("--node_params", type=int, default=1, # I think it should be 0 if each graph has different nodes
            help="Use node-specific parameters for initial state and decay target")
    parser.add_argument("--learn_init_state", type=int, default=1, # I think it should be 0 if each graph has different nodes
            help="If the initial state of GRU-units should be learned (otherwise 0)")

    # Training
    parser.add_argument("--epochs", type=int,
            help="How many epochs to train for", default=10)
    parser.add_argument("--val_interval", type=int, default=1,
            help="Evaluate model every val_interval:th epoch")
    parser.add_argument("--patience", type=int, default=20,
            help="How many evaluations to wait for improvement in val loss")
    parser.add_argument("--lr", type=float,
            help="Learning rate", default=1e-3)
    parser.add_argument("--l2_reg", type=float,
            help="L2-regularization coefficient", default=0.)
    parser.add_argument("--state_updates", type=str, default="obs",
            help="When the node state should be updated (all/obs/hop)")
    parser.add_argument("--loss_weighting", type=str, default="const",
            help="Function to weight loss with, given as: name,param1,...,paramK")
    parser.add_argument("--max_pred", type=int, default=10,
            help="Maximum number of time indices forward to predict")

    args = parser.parse_args(args) # TODO: Changes this when to .py file without argument to read input args
    config = vars(args)

    # Read additional config from file
    if args.config:
        assert os.path.exists(args.config), "No config file: {}".format(args.config)
        with open(args.config) as json_file:
            config_from_file = json.load(json_file)

        # Make sure all options in config file also exist in argparse config.
        # Avoids choosing wrong parameters because of typos etc.
        unknown_options = set(config_from_file.keys()).difference(set(config.keys()))
        unknown_error = "\n".join(["Unknown option in config file: {}".format(opt)
            for opt in unknown_options])
        assert (not unknown_options), unknown_error

        config.update(config_from_file)

    # Some asserts
    assert config["model"] in MODELS, f"Unknown model: {config['model']}"
    assert config["optimizer"] in constants.OPTIMIZERS, (
            f"Unknown optimizer: {config['optimizer']}")

    assert config["gnn_type"] in constants.GNN_LAYERS, (
            f"Unknown gnn_type: {config['gnn_type']}")
    assert (not bool(config["periodic"])) or (config["hidden_dim"] % 2 == 0), (
            "hidden_dim must be even when using periodic latent dynamics")
    
    if config["dtype"] == "float16":
        config["dtype"] = torch.float16
    elif config["dtype"] == "float32":
        config["dtype"] = torch.float32
    else:
        raise ValueError(f"Unknown dtype: {config['dtype']}")

    return config

if __name__ == "__main__":

    config=get_config('')
    if torch.cuda.is_available():
        device = torch.device("cuda")

        # For reproducability on GPU
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False
    else:
        device = torch.device("cpu")
        
    if not USE_CACHED_GRAPH:
        insar = InSAR('data/time_series_Saarland_Orbit139_update.nc')
        master_day = insar.get_master_day()
        gnss = GNSS('data/selected_stations_dates.csv', master_day)

        gnss.filter_signal(insar.sampling_freq/5)
        insar.filter_signal(insar.sampling_freq/5)
        gnss.take_derivative()
        insar.take_derivative()

        insar = insar
        gnss = gnss
        dataset = 'derivative'
        insar_k = 10
        gnss_to_insar_k = 1
        insar_nn_ind_path = 'data/InSAR_data/nearest_neighbors_ind.npy'
        insar_nn_dist_path = 'data/InSAR_data/nearest_neighbors_dist.npy'
        gnss_insar_dist_path = 'data/insar_gnss_distances.npy'
        weight_scaling = 4
        n_divide = 5
        dtype=torch.float32
        device='cpu'

        graph = utils.build_graph(insar,
                        gnss,
                        dataset,
                        insar_k,
                        gnss_to_insar_k,
                        insar_nn_ind_path,
                        insar_nn_dist_path,
                        gnss_insar_dist_path,
                        weight_scaling,\
                        dtype=dtype,
                        device=device
                        )

        with open(os.path.join("data", "cache", "graph.pt"),'wb') as f:
            pickle.dump(graph, f, protocol=pickle.HIGHEST_PROTOCOL)
    else:
        with open(os.path.join("data", "cache", "graph.pt"),'rb') as f:
            graph = pickle.load(f)
        
    loader = ptg.loader.NeighborLoader(graph, batch_size=1, num_neighbors = [-1], pin_memory=True)
    example = next(iter(loader))
    print(example)

    config["num_nodes"] = example.num_nodes
    config["time_steps"] = example.t.shape[1]
    config["device"] = device
    config["y_dim"] = example.y.shape[-1]

    config["has_features"] = hasattr(example, "features") and\
        bool(config["use_features"]) # TODO: not done yet
    if config["has_features"]:
        config["feature_dim"] = example.features.shape[-1]
    else:
        config["feature_dim"] = 0

    config["param_dim"] = 1 #  to simplify we just predict the mean
    loss_weight_func = parse_loss_weight(config["loss_weighting"])

    model = MODELS[config["model"]](config)
    # model.half()
    model.to(device)

    opt = constants.OPTIMIZERS[config["optimizer"]](model.parameters(), lr=config["lr"],
            weight_decay=config["l2_reg"])
    
    batch = next(iter(loader))
    batch.num_graphs = 1
    batch = batch.to(config["device"]) # Move all graphs to GPU

    full_pred_params, pred_delta_times = model.forward(
                batch) # (N_T, B*N, max_pred, d_y, d_param) and (N_T, B, max_pred)
    print("Full pred params: ", full_pred_params.shape)
    print("Pred delta times: ", pred_delta_times.shape)
