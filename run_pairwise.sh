#!/bin/bash

#SBATCH --ntasks=8
#SBATCH --mem-per-cpu=32768
#SBATCH --time=18:00:00
#SBATCH --job-name=insar-dist
#SBATCH --output=slurm/insar-dist-DILL.out
#SBATCH --mail-type=END,FAIL

module load gcc/8.2.0 python/3.11.2
python InSAR_compute_pairwise_distances.py
