import torch.nn as nn
import torch

from .gru_graph_model import GRUGraphModel4Bi

class Bidirectional(nn.Module):
    def __init__(self, config) -> None:
        super(Bidirectional, self).__init__()

        self.gru_model = GRUGraphModel4Bi(config)
        self.gru_model_rev = GRUGraphModel4Bi(config)
        self.ind2keep = config["time_steps"] - config["max_pred"]
        self.max_pred = config["max_pred"]

        first_post_dim = self.gru_model.compute_pred_input_dim(config)
        output_dim = self.gru_model.compute_output_dim(config)

        if config["n_fc"] == 1:
            fc_layers = [nn.Linear(first_post_dim, output_dim)]
        else:
            fc_layers = []
            for layer_i in range(config["n_fc"]-1):
                fc_layers.append(nn.Linear(first_post_dim if (layer_i == 0)
                    else config["hidden_dim"], config["hidden_dim"]))
                fc_layers.append(nn.ReLU())

            fc_layers.append(nn.Linear(config["hidden_dim"], output_dim))

        self.post_layers = nn.Sequential(*fc_layers)
    
    def forward(self, batch):
        
        batch_rev = batch
        batch_rev.y = batch_rev.y.flip(dims=[1])
        batch_rev.mask = batch_rev.mask.flip(dims=[1])
        batch_rev.hop_mask = batch_rev.hop_mask.flip(dims=[1])
        
        full_pred_params, pred_delta_times = self.gru_model.forward(batch)
        full_pred_params_rev, _ = self.gru_model_rev.forward(batch_rev)
        
        
        full_pred_params_rev_flipped = full_pred_params_rev[:self.ind2keep,...].flip(dims=[0,2])
        full_pred_params_rev_padded = nn.functional.pad(full_pred_params_rev_flipped, (0,0,0,0,0,0,0,0,0, self.max_pred), "constant", 0)
        
        summed = full_pred_params + full_pred_params_rev_padded
        
        fc_input = nn.functional.relu(full_pred_params)
        
        output = self.post_layers(fc_input)
        return output, pred_delta_times 
