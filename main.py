import argparse
import os
import json
import numpy as np
import torch
import torch_geometric as ptg
import wandb
import time
import pickle
import copy

from utils import build_graph
from readers.InSAR import InSAR
from readers.GNSS import GNSS
from readers.util import warning_on_one_line_with_timestamp
import warnings
warnings.formatwarning = warning_on_one_line_with_timestamp

import tgnn4i.constants as constants
import tgnn4i.pred_dists as pred_dists
from tgnn4i.gru_model import GRUModel
from tgnn4i.gru_node_model import GRUNodeModel
from tgnn4i.gru_graph_model import GRUGraphModel
from bi_tgnn4i.bidirectional import Bidirectional

from tgnn4i.transformer import TransformerForecaster
from tgnn4i.transformer_joint import TransformerJointForecaster
from tgnn4i.utils import parse_loss_weight
import train


MODELS = {
    "grud_joint": GRUModel, # Ignore graph structure, evolve single joint latent state
    "grud_node": GRUNodeModel, # Treat each node independently, independent latent state
    "tgnn4i": GRUGraphModel, # Utilizes graph structure
    "bi_tgnn4i": Bidirectional,
    "transformer_node": TransformerForecaster,
    "transformer_joint": TransformerJointForecaster,
}

def get_config():
    parser = argparse.ArgumentParser(description='Train Models')
    # If config file should be used
    parser.add_argument("--config", type=str, help="Config file to read run config from")

    # Dataset
    parser.add_argument("--insar_dataset", type=str, default='data/InSAR_data/time_series_Saarland_Orbit139_update.nc',
            help="Path to insar dataset")
    parser.add_argument("--gnss_dataset", type=str, default='data/selected_stations_dates.csv',
            help="Path to gnss dataset")
    parser.add_argument("--dataset_type", type=str, default='derivative',
            help="Type of dataset to use (derivative/filtered/raw)")
    parser.add_argument("--filter_scale", type=float, default=5.,
            help="Filter scale for derivative/filtered data")
    parser.add_argument("--insar_k", type=int, default=10,
            help="Number of nearest neighbors to use for insar nodes")
    parser.add_argument("--gnss_to_insar_k", type=int, default=1,
            help="Number of nearest neighbors to use for gnss nodes")
    parser.add_argument("--insar_nn_ind_path", type=str, default='data/InSAR_data/nearest_neighbors_ind.npy',
            help="Path to insar nearest neighbor indices")
    parser.add_argument("--insar_nn_dist_path", type=str, default='data/InSAR_data/nearest_neighbors_dist.npy',
            help="Path to insar nearest neighbor distances")
    parser.add_argument("--gnss_insar_dist_path", type=str, default='data/insar_gnss_distances.npy',
            help="Path to gnss insar distances")
    parser.add_argument("--dtype", type=str, default="float32",
            help="Data type to use for torch tensors")
    parser.add_argument("--graph_path", type=str, default=None,
                        help="Path to graph to use (overrides dataset)")
    parser.add_argument("--ind_path", type=str, default="data/InSAR_data",
                        help="Path to indices for train/val/test split")
    
    # General
    parser.add_argument("--model", type=str, default="tgnn4i",
            help="Which model to use")
    parser.add_argument("--seed", type=int, default=42,
            help="Seed for random number generator")
    parser.add_argument("--optimizer", type=str, default="adam",
            help="Optimizer to use for training")
    parser.add_argument("--init_points", type=int, default=1,
            help="Number of points to observe before prediction start")
    parser.add_argument("--test", type=int, default=0,
            help="Also evaluate on test set after training is done")
    parser.add_argument("--use_features", type=int, default=1,
            help="If additional input features should be used")
    parser.add_argument("--load", type=str,
            help="Load model parameters from path")
    parser.add_argument("--restart_step", type=int, default=1,
                        help="Step to start from when restarting training")
    parser.add_argument("--wandb_name", type=str, default=None,
            help="Name of wandb run")

    # Model Architecture
    parser.add_argument("--gru_layers", type=int, default=1,
            help="Layers of GRU units")
    parser.add_argument("--decay_type", type=str, default="dynamic",
            help="Parametrization of GRU decay to use (none/to_const/dynamic)")
    parser.add_argument("--periodic", type=int, default=0,
            help="If latent state dynamics should include periodic component")
    parser.add_argument("--time_input", type=int, default=1,
            help="Concatenate time (delta_t) to the input at each timestep")
    parser.add_argument("--mask_input", type=int, default=1,
            help="Concatenate the observation mask as input")
    parser.add_argument("--hidden_dim", type=int, default=32,
            help="Dimensionality of hidden state in GRU units (latent node state))")
    parser.add_argument("--n_fc", type=int, default=2,
            help="Number of fully connected layers after GRU units")
    parser.add_argument("--pred_gnn", type=int, default=1,
            help="Number of GNN-layers to use in predictive part of model")
    parser.add_argument("--gru_gnn", type=int, default=1,
            help="Number of GNN layers used for GRU-cells")
    parser.add_argument("--gnn_type", type=str, default="graphconv",
            help="Type of GNN-layers to use")
    parser.add_argument("--node_params", type=int, default=0, # I think it should be 0 if each graph has different nodes
            help="Use node-specific parameters for initial state and decay target")
    parser.add_argument("--learn_init_state", type=int, default=0, # I think it should be 0 if each graph has different nodes
            help="If the initial state of GRU-units should be learned (otherwise 0)")

    # Training
    parser.add_argument("--epochs", type=int,
            help="How many epochs to train for", default=50)
    parser.add_argument("--val_interval", type=int, default=1,
            help="Evaluate model every val_interval:th epoch")
    parser.add_argument("--patience", type=int, default=20,
            help="How many evaluations to wait for improvement in val loss")
    parser.add_argument("--pred_dist", type=str, default="gauss_fixed",
            help="Predictive distribution")
    parser.add_argument("--metric", type=str, default="mse",
            help="Metric to use for evaluation (mse/nll)")
    parser.add_argument("--lr", type=float,
            help="Learning rate", default=1e-3)
    parser.add_argument("--l2_reg", type=float,
            help="L2-regularization coefficient", default=0.)
    parser.add_argument("--batch_size", type=int,
            help="Batch size", default=32)
    parser.add_argument("--state_updates", type=str, default="obs",
            help="When the node state should be updated (all/obs/hop)")
    parser.add_argument("--loss_weighting", type=str, default="const",
            help="Function to weight loss with, given as: name,param1,...,paramK")
    parser.add_argument("--max_pred", type=int, default=12,
            help="Maximum number of time indices forward to predict")

    args = parser.parse_args()
    config = vars(args)

    # Read additional config from file
    if args.config:
        assert os.path.exists(args.config), "No config file: {}".format(args.config)
        with open(args.config) as json_file:
            config_from_file = json.load(json_file)

        # Make sure all options in config file also exist in argparse config.
        # Avoids choosing wrong parameters because of typos etc.
        unknown_options = set(config_from_file.keys()).difference(set(config.keys()))
        unknown_error = "\n".join(["Unknown option in config file: {}".format(opt)
            for opt in unknown_options])
        assert (not unknown_options), unknown_error

        config.update(config_from_file)

    # Some asserts
    # Some asserts
    assert config["model"] in MODELS, f"Unknown model: {config['model']}"
    assert config["optimizer"] in constants.OPTIMIZERS, (
            f"Unknown optimizer: {config['optimizer']}")
    assert config["pred_dist"] in pred_dists.DISTS, (
            f"Unknown predictive distribution: {config['pred_dist']}")
    assert config["gnn_type"] in constants.GNN_LAYERS, (
            f"Unknown gnn_type: {config['gnn_type']}")
    assert config["init_points"] > 0, "Need to have positive number of init points"
    assert (not bool(config["periodic"])) or (config["hidden_dim"] % 2 == 0), (
            "hidden_dim must be even when using periodic latent dynamics")
    if config["dtype"] == "float16":
        config["dtype"] = torch.float16
    elif config["dtype"] == "float32":
        config["dtype"] = torch.float32
    else:
        raise ValueError(f"Unknown dtype: {config['dtype']}")

    return config

def main():
    config = get_config()

    # Set all random seeds
    np.random.seed(config["seed"])
    torch.manual_seed(config["seed"])

    # Device setup
    if torch.cuda.is_available():
        device = torch.device("cuda")

        # For reproducability on GPU
        torch.backends.cudnn.deterministic = True
        torch.backends.cudnn.benchmark = False
    else:
        device = torch.device("cpu")
    warnings.warn(f"Using device: {device}")

    # Load graph
    if config["graph_path"] is None:
        warnings.warn("Loading data")
        insar = InSAR(config["insar_dataset"])
        insar_master_day = insar.get_master_day()
        gnss = GNSS(config["gnss_dataset"], insar_master_day)

        gnss.filter_signal(insar.sampling_freq/config["filter_scale"])
        insar.filter_signal(insar.sampling_freq/config["filter_scale"])

        gnss.take_derivative()
        insar.take_derivative()

        graph = build_graph(insar,
                            gnss,
                            config["dataset_type"],
                            config["insar_k"],
                            config["gnss_to_insar_k"],
                            config["insar_nn_ind_path"],
                            config["insar_nn_dist_path"],
                            config["gnss_insar_dist_path"],
                            dtype=config["dtype"],
                            device=torch.device("cpu")) # we always use cpu for graph construction
        save_path = os.path.join(os.getenv("SCRATCH"), "DSLab")
        filename = f"graph_{config['insar_k']}_{config['gnss_to_insar_k']}.pt"
        warnings.warn(f"Saving graph to {os.path.join(save_path, filename)}")
        with open(os.path.join(save_path, filename), "wb") as f:
            pickle.dump(graph, f, protocol=pickle.HIGHEST_PROTOCOL)
    else:
        with open(config["graph_path"], "rb") as f:
            graph = pickle.load(f)

    # Create data loader
    train_ind = torch.load(os.path.join(config["ind_path"], "train_ind.pt"))
    val_ind = torch.load(os.path.join(config["ind_path"], "val_ind.pt"))
    test_ind = torch.load(os.path.join(config["ind_path"], "test_ind.pt"))

    # Just to test
    #train_ind = train_ind[:1000]
    #val_ind = val_ind[:1000]
    #test_ind = test_ind[:1000]
    
    train_loader = ptg.loader.NeighborLoader(graph,
                                             num_neighbors=[config["insar_k"]+config["gnss_to_insar_k"]],
                                             input_nodes=train_ind,
                                             batch_size=config["batch_size"],
                                             disjoint=True,
                                             shuffle=True,
                                             pin_memory=True)
    val_loader = ptg.loader.NeighborLoader(graph,
                                             num_neighbors=[config["insar_k"]+config["gnss_to_insar_k"]],
                                             input_nodes=val_ind,
                                             batch_size=config["batch_size"],
                                             disjoint=True,
                                             shuffle=False,
                                             pin_memory=True)
    test_loader = ptg.loader.NeighborLoader(graph,
                                            num_neighbors=[config["insar_k"]+config["gnss_to_insar_k"]],
                                            input_nodes=test_ind,
                                            batch_size=config["batch_size"],
                                            disjoint=True,
                                            shuffle=False,
                                            pin_memory=True)

    # Init wandb
    if config["wandb_name"] is None:
        wandb_name = f"insar_k_{config['insar_k']}_gnss_to_insar_k_{config['gnss_to_insar_k']}_{time.strftime('%H-%M-%S')}"
    else:
        wandb_name = config["wandb_name"]
    wandb.init(project=constants.WANDB_PROJECT, name=wandb_name, config=config)
    # Additional config needed for some model setup (need/should not be logged to wandb)
    config["num_nodes"] = 1 + config["insar_k"] + config["gnss_to_insar_k"]
    config["time_steps"] = graph.t.shape[1]
    config["device"] = device
    config["y_dim"] = graph.y.shape[-1]

    config["has_features"] = hasattr(graph, "features") and\
        bool(config["use_features"])
    if config["has_features"]:
        config["feature_dim"] = graph.features.shape[-1]
    else:
        config["feature_dim"] = 0

    # param_dim is number of parameters in predictive distribution
    pred_dist, config["param_dim"] = pred_dists.DISTS[config["pred_dist"]]

    print("Config:")
    for key, val in config.items():
        print(f"{key}: {val}")

    # Parse loss weighting function
    loss_weight_func = parse_loss_weight(config["loss_weighting"])

    # Create model, optimizer
    model = MODELS[config["model"]](config).to(device)
    if config["load"]:
        model.load_state_dict(torch.load(config["load"], map_location=device))
        print(f"Parameters loaded from: {config['load']}")
        start_epoch = config["restart_step"]
    else:
        start_epoch = 1
    opt = constants.OPTIMIZERS[config["optimizer"]](model.parameters(), lr=config["lr"],
            weight_decay=config["l2_reg"])
    
    train_epoch = train.train_epoch
    val_epoch = train.val_epoch
    test_epoch = train.val_epoch

    # Train model
    best_val_loss = np.inf
    best_val_metrics = None
    best_val_epoch = -1 # Index of the best epoch
    best_params = None

    model.train()
    for epoch_i in range(start_epoch, config["epochs"]+1):
        print(f"Epoch {epoch_i}:")
        epoch_train_loss = train_epoch(model, train_loader, opt, pred_dist,
                config, loss_weight_func)
        
        # save after each epoch
        param_save_path = f"{wandb.run.dir}/epoch_{epoch_i}.pt"
        torch.save(best_params, param_save_path)

        if (epoch_i % config["val_interval"]== 0):
            # Validate, evaluate
            with torch.no_grad():
                epoch_val_metrics = val_epoch(model, val_loader, pred_dist,
                        loss_weight_func, config)

            log_metrics = {"train_loss": epoch_train_loss}
            log_metrics.update({f"val_{metric}": val for
                metric, val in epoch_val_metrics.items()})

            epoch_val_loss = log_metrics["val_mse"] # Use mse as main metric
            epoch_val_nll = log_metrics["val_nll"]

            print(f"Epoch {epoch_i}:\t train_loss: {epoch_train_loss:.6f} "\
                    f"\tval_mse: {epoch_val_loss:.6f} \tval_nll: {epoch_val_nll:.6f}")

            wandb.log(log_metrics, step=epoch_i, commit=True)

            if epoch_val_loss < best_val_loss:
                best_val_loss = epoch_val_loss
                best_val_metrics = log_metrics
                best_val_epoch = epoch_i
                best_params = copy.deepcopy(model.state_dict())

            if (epoch_i - best_val_epoch)/config["val_interval"] >= config["patience"]:
                # No improvement, end training
                print("Val loss no longer improving, stopping training.")
                break
        
    # Save things
    param_save_path = os.path.join(wandb.run.dir, constants.PARAM_FILE_NAME)
    torch.save(best_params, param_save_path)
    # Wandb summary
    del best_val_metrics["train_loss"] # Exclude this one from summary update
    for metric, val in best_val_metrics.items():
        wandb.run.summary[metric] = val

    # (Optionally) Evaluate on test set
    if config["test"]:
        with torch.no_grad():
            test_metrics = test_epoch(model, test_loader, pred_dist,
                    loss_weight_func, config)
            test_metric_dict = {f"test_{name}": val
                    for name, val in test_metrics.items()}
            wandb.run.summary.update(test_metric_dict)

            print("Test set evaluation:")
            for name, val in test_metric_dict.items():
                print(f"{name}:\t {val}")


if __name__ == "__main__":
    main()