#!/bin/bash

#SBATCH --ntasks=2
#SBATCH --mem-per-cpu=32768
#SBATCH --time=145:00:00
#SBATCH --job-name=insar-superresolution-nll-bidirectional
#SBATCH --output=slurm/training_nll_bidirectional.out
#SBATCH --mail-type=END,FAIL
#SBATCH --gpus=rtx_4090:1

module load gcc/8.2.0 python_gpu/3.10.4
module load eth_proxy
python main.py --model bi_tgnn4i --batch_size 34 --graph_path /cluster/scratch/jlucki/insar_super_resolution/graph_10_1.pt --wandb_name nll_bidirectional --metric nll --pred_dist gauss