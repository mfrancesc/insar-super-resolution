from readers.InSAR import InSAR
from readers.GNSS import GNSS
from readers.util import warning_on_one_line
from tgnn4i.utils import compute_edge_weights
from readers.util import node_t_deltas

import torch
import torch.nn as nn
import warnings
from tqdm import tqdm
import numpy as np
import torch_geometric as ptg
import geopandas as gpd
from shapely.geometry import Polygon
import math

warnings.formatwarning = warning_on_one_line

def get_edges_and_distances(insar_nn_ind,
                            insar_nn_dist, 
                            gnss_insar_dist, 
                            insar_k, 
                            gnss_to_insar_k = 1, # number of gnss nodes to connect to each insar node (-1 means all)
                            dtype=torch.float32,
                            device='cpu'):
    """
    Get edges and edge weights for the graph
    insar_nn_ind: (n_insar, max_insar_k) array of indices of nearest neighbors
    insar_nn_dist: (n_insar, max_insar_k) array of distances to nearest neighbors
    insar_k: number of nearest neighbors to use
    gnss_insar_dist: (n_insar, n_gnss) array of distances from insar nodes to gnss nodes
    dtype: torch datatype
    device: torch device
    Returns:
    edge_index: (2, n_edges) array of edges
    edge_distances: (n_edges,1) array of edge distances
    n_nodes: number of nodes in the graph
    """
    assert insar_nn_ind.shape[0] == insar_nn_dist.shape[0]
    assert insar_k > 0
    assert insar_k <= insar_nn_ind.shape[1]
    assert insar_nn_ind.shape[0] == gnss_insar_dist.shape[0]

    # Get edges and edge distances
    n_insar = insar_nn_ind.shape[0]
    n_gnss = gnss_insar_dist.shape[1]
    warnings.warn(f"Computing edges and edge distances for insar nodes, there are {n_insar} insar nodes and {n_gnss} gnss nodes")
    edges = []
    edge_distances = []
    for i in tqdm(range(n_insar)):
        selected_nodes = insar_nn_ind[i, :insar_k]
        for idx, j in enumerate(selected_nodes):
            # add edge in direction j->i
            edges.append([j,i])
            edge_distances.append(insar_nn_dist[i,idx])
        # add edge from gnss to insar
        if gnss_to_insar_k == -1:
            selected_gnss_nodes = range(n_gnss)
        else:
            selected_gnss_nodes = gnss_insar_dist[i,:].argsort()[:gnss_to_insar_k]
        for j in selected_gnss_nodes:
            edges.append([n_insar+j, i])
            edge_distances.append(gnss_insar_dist[i, j])
        
    edges = torch.tensor(edges, dtype=torch.long, device=device).t().contiguous()
    edge_distances = torch.tensor(edge_distances, dtype=dtype, device=device).unsqueeze(1)
    n_nodes = n_insar + n_gnss
    return edges, edge_distances, n_nodes

def build_graph(insar,
                gnss,
                dataset,
                insar_k,
                gnss_to_insar_k,
                insar_nn_ind_path,
                insar_nn_dist_path,
                gnss_insar_dist_path,
                dtype=torch.float32,
                device='cpu'
                ):
    
    insar_nn_ind = np.load(insar_nn_ind_path)
    insar_nn_dist = np.load(insar_nn_dist_path)
    gnss_insar_dist = np.load(gnss_insar_dist_path)
    gnss_df = gnss.get_df()
    if dataset == 'derivative':
        insar_df = insar.derivative_df
        gnss_df = gnss_df[['site', 'day', 'st_up_derivative(mm)']]
    elif dataset == 'filtered':
        warnings.warn("Using filtered dataset, make sure this is what you want")
        insar_df = insar.filtered_df
        gnss_df = gnss_df[['site', 'day', 'st_up_filtered(mm)']]
    elif dataset == 'raw':
        warnings.warn("Using raw dataset, make sure this is what you want")
        insar_df = insar.df
        gnss_df = gnss_df[['site', 'day', 'st_up_no_year(mm)']]
    else:
        raise ValueError(f"Unknown dataset {dataset}")
    
    if insar_df is None:
        raise ValueError(f"Dataset {dataset} is not available for InSAR")
    gnss_df.columns = ['site', 'day', 'value']
    
    assert insar_df.shape[0] == insar_nn_ind.shape[0]
    assert insar_df.shape[0] == insar_nn_dist.shape[0]
    assert insar_df.shape[0] == gnss_insar_dist.shape[0]

    edges, edge_distances, num_nodes = get_edges_and_distances(insar_nn_ind,
                                                             insar_nn_dist,
                                                             gnss_insar_dist,
                                                             insar_k,
                                                             gnss_to_insar_k = gnss_to_insar_k,
                                                             dtype=dtype,
                                                             device=device)
    
    edge_weights = 1/torch.log(5*edge_distances/1000 + math.e)
    
    assert edges.shape[0] == 2
    assert edge_weights.shape[0] == edges.shape[1]
    assert edge_weights.shape[1] == 1

    # We now compute the needed tensors for the graph
    warnings.warn("Computing tensors for graph")
    insar_y, insar_mask, insar_ts, insar_delta_ts, insar_first_day, insar_last_day = InSAR.get_data_for_graph(insar_df, dtype=dtype, device=device)
    gnss_y, gnss_mask, gnss_ts, gnss_delta_ts = GNSS.get_data_for_graph(gnss_df, insar_first_day, insar_last_day, dtype=dtype, device=device)

    y = torch.cat([insar_y, gnss_y], dim=0)
    mask = torch.cat([insar_mask, gnss_mask], dim=0)
    delta_ts = torch.cat([insar_delta_ts, gnss_delta_ts], dim=0)
    assert (gnss_ts == insar_ts).all()
    ts = insar_ts

    # create features (two binary variables for each node, 1,0 if it is a GNSS node and 0,1 if it is an InSAR node)
    features = torch.zeros((num_nodes, ts.shape[0], 4), dtype=dtype, device=device)
    features[:insar_y.shape[0],:,1] = 1
    features[insar_y.shape[0]:,:,0] = 1

    # add geographical features (we project to EPSG:32632 and normalize)
    insar_geometry = gpd.points_from_xy(insar_df['longitude'], insar_df['latitude'])
    insar_geometry.crs = "EPSG:4326"
    insar_geometry_conv = insar_geometry.to_crs("EPSG:32632")
    bounds = insar_geometry_conv.total_bounds # minx, miny, maxx, maxy
    x_coord = (insar_geometry_conv.x - bounds[0]) / (bounds[2] - bounds[0])
    y_coord = (insar_geometry_conv.y - bounds[1]) / (bounds[3] - bounds[1])
    features[:insar_y.shape[0],:,2] = torch.tensor(x_coord).unsqueeze(1)
    features[:insar_y.shape[0],:,3] = torch.tensor(y_coord).unsqueeze(1)

    # add GNSS station features
    site_gdf = gnss.get_sites_gdf()
    assert (np.sort(site_gdf.index.values) == site_gdf.index.values).all()
    site_gdf = site_gdf.to_crs("EPSG:32632") # https://epsg.io/32632
    gnss_geometry = site_gdf.geometry
    x_coord = (gnss_geometry.x - bounds[0]) / (bounds[2] - bounds[0])
    y_coord = (gnss_geometry.y - bounds[1]) / (bounds[3] - bounds[1])
    features[insar_y.shape[0]:,:,2] = torch.tensor(x_coord.values).unsqueeze(1)
    features[insar_y.shape[0]:,:,3] = torch.tensor(y_coord.values).unsqueeze(1)

    
    # remove isolated nodes
    warnings.warn("Removing isolated nodes")
    edges, edge_weights, isolation_mask = ptg.utils.remove_isolated_nodes(edges,
        edge_weights, num_nodes=num_nodes)
    n_removed = (isolation_mask == False).sum().item()
    if n_removed > 0:
        warnings.warn(f"Removed {n_removed} isolated nodes")
        removed_nodes_idx = torch.nonzero(isolation_mask[insar_y.shape[0]:] == False).squeeze(1)
        removed_nodes = site_gdf.iloc[removed_nodes_idx.cpu().numpy()].index.values
        warnings.warn(f"Removed GNSS stations {removed_nodes}")
        y = y[isolation_mask]
        mask = mask[isolation_mask]
        delta_ts = delta_ts[isolation_mask]
        features = features[isolation_mask]
        num_nodes = isolation_mask.sum().item()
    else:
        warnings.warn("No isolated nodes found")

    # create graph
    graph = ptg.data.Data(edge_index=edges,
                         edge_attr=edge_weights, 
                         y=y,
                         features=features,
                         mask=mask,
                         t=ts.unsqueeze(0),
                         delta_t=delta_ts,
                         hop_mask=mask, # not sure 
                         update_delta_t=delta_ts,
                         num_nodes=num_nodes)
    return graph

def divide_polygon(bbox, n):
    # Divide the x-axis and y-axis into n equivalent spaces
    x_divisions = np.linspace(bbox[0], bbox[2], n + 1)
    y_divisions = np.linspace(bbox[1], bbox[3], n + 1)
    polygons = []
    for x_i in range(n):
        for y_i in range(n):
            new_poly = Polygon([(x_divisions[x_i], y_divisions[y_i]),
                                (x_divisions[x_i + 1], y_divisions[y_i]),
                                (x_divisions[x_i + 1], y_divisions[y_i + 1]),
                                (x_divisions[x_i], y_divisions[y_i + 1])
                               ])
            polygons.append(new_poly)

    return polygons

def add_rows_until_max(original_tensor, n_rows):
    rows_to_add = max(0, n_rows - original_tensor.size(0))

    # Create a tensor with zeros to add
    zeros_to_add_shape = (rows_to_add,) + original_tensor.size()[1:]  # Keep the dimensions from 2 onwards
    zeros_to_add = torch.zeros(zeros_to_add_shape, dtype=original_tensor.dtype, device=original_tensor.device)

    # Concatenate the original tensor with zeros
    result_tensor = torch.cat((original_tensor, zeros_to_add), dim=0)
    return result_tensor

def build_graphs_divide_grid(insar,
                gnss,
                dataset,
                insar_k,
                gnss_to_insar_k,
                insar_nn_ind_path,
                insar_nn_dist_path,
                gnss_insar_dist_path,
                weight_scaling,
                n_divide,
                dtype=torch.float32,
                device='cpu'
                ):
    insar_nn_ind = np.load(insar_nn_ind_path)
    insar_nn_dist = np.load(insar_nn_dist_path)
    gnss_insar_dist = np.load(gnss_insar_dist_path)
    gnss_df = gnss.get_df()
    if dataset == 'derivative':
        insar_df = insar.derivative_df
        gnss_df = gnss_df[['site', 'day', 'st_up_derivative(mm)']]
    elif dataset == 'filtered':
        warnings.warn("Using filtered dataset, make sure this is what you want")
        insar_df = insar.filtered_df
        gnss_df = gnss_df[['site', 'day', 'st_up_filtered(mm)']]
    elif dataset == 'raw':
        warnings.warn("Using raw dataset, make sure this is what you want")
        insar_df = insar.df
        gnss_df = gnss_df[['site', 'day', 'st_up_no_year(mm)']]
    else:
        raise ValueError(f"Unknown dataset {dataset}")
    
    if insar_df is None:
        raise ValueError(f"Dataset {dataset} is not available for InSAR")
    gnss_df.columns = ['site', 'day', 'value']
    
    assert insar_df.shape[0] == insar_nn_ind.shape[0]
    assert insar_df.shape[0] == insar_nn_dist.shape[0]
    assert insar_df.shape[0] == gnss_insar_dist.shape[0]

    edges, edge_distances, num_nodes = get_edges_and_distances(insar_nn_ind,
                                                             insar_nn_dist,
                                                             gnss_insar_dist,
                                                             insar_k,
                                                             gnss_to_insar_k = gnss_to_insar_k,
                                                             dtype=dtype,
                                                             device=device)
    
    # TODO: need to think of a way to pass from distances to weights, compute_edge_weights has a problem as the distances with respect to the gnss nodes are much larger than the distances with respect to the insar nodes 
    edge_weights = compute_edge_weights(edge_distances, weight_scaling, dtype=dtype)
    
    assert edges.shape[0] == 2
    assert edge_weights.shape[0] == edges.shape[1]
    assert edge_weights.shape[1] == 1

    # We now compute the needed tensors for the graph
    warnings.warn("Computing tensors for graph")
    insar_y, insar_mask, insar_ts, insar_delta_ts, insar_first_day, insar_last_day = InSAR.get_data_for_graph(insar_df, dtype=dtype, device=device)
    gnss_y, gnss_mask, gnss_ts, gnss_delta_ts = GNSS.get_data_for_graph(gnss_df, insar_first_day, insar_last_day, dtype=dtype, device=device)

    insar_geometry = gpd.points_from_xy(insar_df['longitude'], insar_df['latitude'])
    insar_geometry.crs = "EPSG:4326"
    insar_geometry_conv = insar_geometry.to_crs("EPSG:32632") 
    
    bbox = insar_geometry_conv.total_bounds # minx, miny, maxx, maxy
    polygons = divide_polygon(bbox, n_divide)

    # Create GeoDataFrames from GeoSeries
    polygons_gdf = gpd.GeoDataFrame(geometry=polygons, crs="EPSG:32632")
    points_gdf = gpd.GeoDataFrame(geometry=insar_geometry_conv, crs="EPSG:32632")

    # Perform spatial join
    insar_to_grid = gpd.sjoin(points_gdf, polygons_gdf, how="left", predicate="covered_by")

    assert insar_to_grid.shape[0] == insar_geometry_conv.shape[0]

    max_n_points = insar_to_grid.index_right.value_counts().max()

    gnss_start = insar_df.shape[0]
    graphs = []
    for chosen_polygon in tqdm(range(len(polygons_gdf))):
        chosen_points = insar_to_grid.index_right == chosen_polygon
        n_points_in_polygon = insar_to_grid.index_right.value_counts()[chosen_polygon]
        
        chosen_insar_y = insar_y[chosen_points]
        chosen_insar_mask = insar_mask[chosen_points]
        chosen_insar_delta_ts = insar_delta_ts[chosen_points]
        
        expanded_y = add_rows_until_max(chosen_insar_y, max_n_points)
        expanded_mask = add_rows_until_max(chosen_insar_mask, max_n_points)
        expanded_delta_ts = node_t_deltas(insar_ts.unsqueeze(0),
                expanded_mask.unsqueeze(0).transpose(1,2),
                dtype=dtype,
                device=device).squeeze(0).transpose(0,1)
        
        assert (chosen_insar_delta_ts == expanded_delta_ts[:n_points_in_polygon]).all()
        
        chosen_points_idx = np.nonzero(chosen_points.values)[0]
        keep_edges_bool = torch.logical_and(
            torch.isin(edges[1], torch.LongTensor(chosen_points_idx)), # the ones that finish at some point in the chose
            torch.logical_or( # the ones that start at some point in the chosen or in a GNSS station
                torch.isin(edges[0], torch.LongTensor(chosen_points_idx)),
                edges[0] > gnss_start
            ))
        kept_edges = edges[:,keep_edges_bool]
        renamed_edges = torch.searchsorted(torch.LongTensor(chosen_points_idx), kept_edges)
        
        renamed_edges[renamed_edges == n_points_in_polygon] = kept_edges[renamed_edges == n_points_in_polygon] - gnss_start + max_n_points
        
        y = torch.cat([expanded_y, gnss_y], dim=0)
        mask = torch.cat([expanded_mask, gnss_mask], dim=0)
        delta_ts = torch.cat([expanded_delta_ts, gnss_delta_ts], dim=0)
        ts = insar_ts
        selected_edge_weights = edge_weights[keep_edges_bool]
        graph = ptg.data.Data(edge_index=renamed_edges,
                            edge_attr=selected_edge_weights, 
                            y=y,
                            mask=mask,
                            t=ts.unsqueeze(0),
                            delta_t=delta_ts,
                            hop_mask=mask, # not sure 
                            update_delta_t=delta_ts,
                            num_nodes=max_n_points + gnss_y.shape[0])
        graphs.append(graph)

    return graphs