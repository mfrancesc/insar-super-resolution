#!/bin/bash

#SBATCH --ntasks=8
#SBATCH --mem-per-cpu=32768
#SBATCH --time=1:00:00
#SBATCH --job-name=insar-dist
#SBATCH --output=slurm/insar-gnss-dist.out
#SBATCH --mail-type=END,FAIL

module load gcc/8.2.0 python/3.11.2
python compute_insar_gnss_distances.py
